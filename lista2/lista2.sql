--Zadanie 17
SELECT      pseudo "Pracuje w polu",
            przydzial_myszy "Przydzial myszy",
            nazwa "Banda"
FROM        Kocury
INNER JOIN  Bandy ON Bandy.nr_bandy = Kocury.nr_bandy
WHERE       teren IN ('POLE', 'CALOSC')
AND         Kocury.przydzial_myszy > 50;

--Zadanie 18
SELECT      k1.imie,
            k1.w_stadku_od "Poluje od"
FROM        Kocury k1
INNER JOIN  Kocury k2 ON k2.imie = 'JACEK'
WHERE       k1.w_stadku_od < k2.w_stadku_od;

--Zadanie 19a
SELECT      k1.imie,
            ' | ' AS " ",
            k1.funkcja,
            ' | ' AS " ",
            NVL(k2.imie, ' ')    "Szef 1",
            ' | ' AS " ",
            NVL(k3.imie, ' ')    "Szef 2",
            ' | ' AS " ",
            NVL(k4.imie, ' ')    "Szef 3"
FROM        Kocury k1
LEFT JOIN   Kocury k2   ON k2.pseudo = k1.szef
LEFT JOIN   Kocury k3   ON k3.pseudo = k2.szef
LEFT JOIN   Kocury k4   ON k4.pseudo = k3.szef
WHERE       k1.funkcja  IN ('KOT', 'MILUSIA');

--Zadanie 19b
SELECT CONNECT_BY_ROOT imie AS "Imie",
        ' | ' " ",
        funkcja AS "Funkcja",
        ' | ' " ",
        NVL((
            SELECT      imie
            FROM        Kocury 
            WHERE       LEVEL = 2 
            CONNECT BY  PRIOR szef = pseudo 
            START WITH  pseudo = K.pseudo
        ), ' ') AS "Szef 1",
        ' | ' " ",
        NVL((
            SELECT      imie
            FROM        Kocury 
            WHERE       LEVEL = 3 
            CONNECT BY  PRIOR szef = pseudo
            START WITH  pseudo = K.pseudo
        ), ' ') AS "Szef 2",
        ' | ' " ",
        NVL((
            SELECT      imie
            FROM        Kocury 
            WHERE       LEVEL = 4
            CONNECT BY  PRIOR szef = pseudo 
            START WITH  pseudo = K.pseudo
        ) , ' ') AS "Szef 3"
FROM        Kocury K
WHERE       funkcja IN ('KOT', 'MILUSIA')
CONNECT BY  PRIOR szef = pseudo 
START WITH  funkcja IN ('KOT', 'MILUSIA');


SELECT  imieRoot imie,
        funkcja,
        NVL(s2,' ') "SZEF 1",
        NVL(s3,' ') "SZEF 2",
        NVL(s4,' ') "SZEF 3"
FROM (
    SELECT      CONNECT_BY_ROOT imie imieRoot,
                CONNECT_BY_ROOT funkcja funkcja,
                LEVEL lvl,
                imie
    FROM        Kocury k
    CONNECT BY  PRIOR szef = pseudo 
    START WITH  funkcja IN ('KOT', 'MILUSIA')
)
PIVOT (
  MIN(imie) for lvl in (2 s2, 3 s3, 4 s4)
);

--Zadanie 19c
SELECT      imie,
            ' | ' AS " ",
            funkcja,
            RTRIM(REVERSE(RTRIM(SYS_CONNECT_BY_PATH(REVERSE(imie), ' | '), imie)), '| ') AS "Imiona kolejnych szefow"
FROM        Kocury
WHERE       funkcja IN ('KOT', 'MILUSIA')
CONNECT BY  PRIOR pseudo = szef
START WITH  szef is null;

--Zadanie 19c
--1
SELECT      imie,
            ' | ' AS " ",
            funkcja,
            LEVEL
FROM        Kocury
CONNECT BY  PRIOR pseudo = szef
START WITH  szef is null;

--2
SELECT      imie,
            ' | ' AS " ",
            funkcja,
            SYS_CONNECT_BY_PATH(imie, ' | ') AS "Imiona kolejnych szefow"
FROM        Kocury
WHERE       funkcja IN ('KOT', 'MILUSIA')
CONNECT BY  PRIOR pseudo = szef
START WITH  szef is null;

--3
SELECT      imie,
            ' | ' AS " ",
            funkcja,
            RTRIM(SYS_CONNECT_BY_PATH(imie, ' | '), imie) AS "Imiona kolejnych szefow"
FROM        Kocury
WHERE       funkcja IN ('KOT', 'MILUSIA')
CONNECT BY  PRIOR pseudo = szef
START WITH  szef is null;

--4
SELECT      imie,
            ' | ' AS " ",
            funkcja,
            REVERSE(RTRIM(SYS_CONNECT_BY_PATH(REVERSE(imie), ' | '), imie)) AS "Imiona kolejnych szefow"
FROM        Kocury
WHERE       funkcja IN ('KOT', 'MILUSIA')
CONNECT BY  PRIOR pseudo = szef
START WITH  szef is null;

--5
SELECT      imie,
            ' | ' AS " ",
            funkcja,
            RTRIM(REVERSE(RTRIM(SYS_CONNECT_BY_PATH(REVERSE(imie), ' | '), imie)), '| ') AS "Imiona kolejnych szefow"
FROM        Kocury
WHERE       funkcja IN ('KOT', 'MILUSIA')
CONNECT BY  PRIOR pseudo = szef
START WITH  szef is null;

--Zadanie 20
SELECT      k.imie,
            b.nazwa,
            w.imie_wroga,
            w.stopien_wrogosci,
            wk.data_incydentu
FROM        Kocury k
INNER JOIN  Bandy b             ON b.nr_bandy   = k.nr_bandy
INNER JOIN  Wrogowie_Kocurow wk ON wk.pseudo    = k.pseudo
INNER JOIN  Wrogowie w          ON w.imie_wroga = wk.imie_wroga
WHERE       k.plec = 'D'
AND         EXTRACT(YEAR FROM wk.data_incydentu) >= 2007;

--Zadanie 21
SELECT      b.nazwa,
            COUNT(DISTINCT k.pseudo)
FROM        Kocury k
INNER JOIN  Bandy b             ON b.nr_bandy   = k.nr_bandy
INNER JOIN  Wrogowie_Kocurow wk ON wk.pseudo    = k.pseudo
GROUP BY    b.nazwa;

--Zadanie 22
SELECT      k.funkcja,
            wk.pseudo,
            COUNT(wk.pseudo)
FROM        Kocury k
INNER JOIN  Wrogowie_Kocurow wk ON wk.pseudo    = k.pseudo
GROUP BY    wk.pseudo, k.funkcja
HAVING      COUNT(wk.pseudo) >= 2;

--Zadanie 23
SELECT      imie,
            (przydzial_myszy + myszy_extra) * 12 "DAWKA ROCZNA",
            'Ponizej 864'
FROM        Kocury
WHERE       (przydzial_myszy + myszy_extra) * 12 < 864
UNION
SELECT      imie,
            (przydzial_myszy + myszy_extra) * 12 "DAWKA ROCZNA",
            '864'
FROM        Kocury
WHERE       (przydzial_myszy + myszy_extra) * 12 = 864
UNION
SELECT      imie,
            (przydzial_myszy + myszy_extra) * 12 "DAWKA ROCZNA",
            'Powyzej 864'
FROM        Kocury
WHERE       (przydzial_myszy + myszy_extra) * 12 > 864
ORDER BY    "DAWKA ROCZNA" DESC;

--Zadanie 24a
SELECT      nr_bandy  "Nr bandy",
            nazwa,
            teren
FROM        Bandy 
LEFT JOIN   Kocury    USING(nr_bandy)
WHERE       pseudo IS NULL;

--Zadanie 24b
SELECT      nr_bandy  "Nr bandy",
            nazwa,
            teren
FROM        Bandy
WHERE       nr_bandy NOT IN (SELECT nr_bandy FROM Kocury);

SELECT nr_bandy, nazwa, teren FROM Bandy
MINUS
SELECT nr_bandy, nazwa, teren FROM Kocury NATURAL JOIN Bandy;

SELECT  nr_bandy,
        nazwa,
        teren 
FROM    (SELECT nr_bandy FROM Bandy
            MINUS
        SELECT nr_bandy FROM Kocury)
NATURAL JOIN Bandy;

--Zadanie 25
SELECT  imie,
        funkcja,
        przydzial_myszy "Przydzial myszy"
FROM    Kocury
WHERE   przydzial_myszy >= ALL(
            SELECT  3 * przydzial_myszy 
            FROM    Kocury NATURAL JOIN Bandy
            WHERE   teren IN ('SAD','CALOSC') AND funkcja = 'MILUSIA'
        );

--Zadanie 26
SELECT      funkcja,
            ROUND(AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0))) "Srednio najw. i najm. myszy"
FROM        Funkcje NATURAL JOIN Kocury
GROUP BY    funkcja
HAVING      AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) IN (
                (SELECT         MAX(AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)))
                FROM            Funkcje NATURAL JOIN Kocury
                WHERE           funkcja != 'SZEFUNIO'
                GROUP BY        funkcja),
                (SELECT         MIN(AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)))
                FROM            Funkcje NATURAL JOIN Kocury
                WHERE           funkcja != 'SZEFUNIO'
                GROUP BY        funkcja)
            );      

--Zadanie 27a
SELECT      pseudo,
            (nvl(k.przydzial_myszy,0) + nvl(k.myszy_extra,0)) "ZJADA"
FROM        Kocury k
WHERE       &howMany >= (
                SELECT  COUNT(nvl(przydzial_myszy,0) + nvl(myszy_extra,0))
                FROM    Kocury
                WHERE   nvl(k.przydzial_myszy,0) + nvl(k.myszy_extra,0) < nvl(przydzial_myszy,0) + nvl(myszy_extra,0)
            )
ORDER BY 2 DESC;

--Zadanie 27b
SELECT      pseudo,
            NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0) "ZJADA"
FROM        Kocury 
WHERE       NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0) IN (
                SELECT  * 
                FROM    (
                            SELECT      DISTINCT NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)
                            FROM        Kocury
                            ORDER BY    1 DESC
                        )
                WHERE   ROWNUM <= &howMany
            );

--Zadanie 27c
SELECT              k1.pseudo,
                    MIN(NVL(k1.przydzial_myszy, 0) + NVL(k1.myszy_extra, 0)) "ZJADA"
FROM                Kocury k1 
FULL OUTER JOIN     Kocury k2 ON NVL(k1.przydzial_myszy, 0) + NVL(k1.myszy_extra, 0) < NVL(k2.przydzial_myszy, 0) + NVL(k2.myszy_extra, 0)
GROUP BY            k1.pseudo
HAVING              COUNT(DISTINCT NVL(k2.przydzial_myszy, 0) + NVL(k2.myszy_extra, 0)) < &howMany AND k1.pseudo IS NOT NULL
ORDER BY            2 DESC;

select * from funkcje f1 join funkcje f2 on 1=1;

--Zadanie 27d
SELECT  pseudo,
        zjada
FROM (
    SELECT  pseudo,
            NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0) ZJADA,
            DENSE_RANK() OVER (
                ORDER BY NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0) DESC
            ) RANK
    FROM    Kocury
)
WHERE RANK <= &howMany;

--Zadanie 28
SELECT      EXTRACT(YEAR FROM w_stadku_od) || '',
            COUNT(pseudo)
FROM        Kocury
GROUP BY    EXTRACT(YEAR FROM w_stadku_od)
HAVING      COUNT(pseudo) IN (
                (
                    SELECT * FROM
                        (SELECT      DISTINCT COUNT(pseudo)
                         FROM        Kocury
                         GROUP BY    EXTRACT(YEAR FROM w_stadku_od)
                         HAVING      COUNT(pseudo) > (
                                        SELECT      AVG(COUNT(EXTRACT(YEAR FROM w_stadku_od)))
                                        FROM        Kocury
                                        GROUP BY    EXTRACT(YEAR FROM w_stadku_od)
                                    )
                         ORDER BY    COUNT(pseudo))
                    WHERE       ROWNUM = 1
                ),
                (
                    SELECT * FROM
                        (SELECT      DISTINCT COUNT(pseudo)
                         FROM        Kocury
                         GROUP BY    EXTRACT(YEAR FROM w_stadku_od)
                         HAVING      COUNT(pseudo) < (
                                        SELECT      AVG(COUNT(EXTRACT(YEAR FROM w_stadku_od)))
                                        FROM        Kocury
                                        GROUP BY    EXTRACT(YEAR FROM w_stadku_od)
                                    )
                         ORDER BY    COUNT(pseudo) DESC)
                    WHERE       ROWNUM = 1
                 )
            )
UNION
SELECT      '�rednia',
            ROUND(AVG(COUNT(EXTRACT(YEAR FROM w_stadku_od))), 7)
FROM        Kocury
GROUP BY    EXTRACT(YEAR FROM w_stadku_od)
ORDER BY    2;

--Zadanie 29a  
SELECT      k1.imie,
            MIN(NVL(k1.przydzial_myszy, 0) + NVL(k1.myszy_extra, 0)) "ZJADA",
            MIN(k1.nr_bandy) "NR BANDY",
            TO_CHAR(AVG(NVL(k2.przydzial_myszy, 0) + NVL(k2.myszy_extra, 0)), '99.99') "SREDNIA BANDY"
FROM        Kocury k1 
JOIN        Kocury k2 ON k1.nr_bandy = k2.nr_bandy
WHERE       k1.plec = 'M'
GROUP BY    k1.imie
HAVING      MIN(NVL(k1.przydzial_myszy, 0) + NVL(k1.myszy_extra, 0)) < AVG(NVL(k2.przydzial_myszy, 0) + NVL(k2.myszy_extra, 0));

--Zadanie 29b
SELECT          imie,
                NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0) "ZJADA",
                nr_bandy "NR BANDY",
                TO_CHAR(srednia, '99.99') "SREDNIA BANDY"
FROM            (SELECT nr_bandy, AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) srednia FROM Kocury GROUP BY nr_bandy)
NATURAL JOIN    Kocury
WHERE           NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0) < srednia
AND             plec = 'M';

--Zadanie 29c
SELECT      k1.imie,
            NVL(k1.przydzial_myszy, 0) + NVL(k1.myszy_extra, 0) "ZJADA",
            k1.nr_bandy "NR BANDY",
            TO_CHAR((
                SELECT  AVG(NVL(k2.przydzial_myszy, 0) + NVL(k2.myszy_extra, 0))
                FROM    Kocury k2 
                WHERE   k1.nr_bandy = k2.nr_bandy
            ), '99.99') "SREDNIA BANDY"
FROM        Kocury k1
WHERE       NVL(k1.przydzial_myszy, 0) + NVL(k1.myszy_extra, 0) < (
                SELECT  AVG(NVL(k3.przydzial_myszy, 0) + NVL(k3.myszy_extra, 0))
                FROM    Kocury k3 
                WHERE   k1.nr_bandy = k3.nr_bandy
            )
AND         plec = 'M';

--Zadanie 30
SELECT      imie,
            w_stadku_od "WSTAPIL DO STADKA",
            ' ' " "
FROM        Kocury k
WHERE       w_stadku_od != (SELECT MAX(w_stadku_od) FROM Kocury WHERE k.nr_bandy = nr_bandy)
AND         w_stadku_od != (SELECT MIN(w_stadku_od) FROM Kocury WHERE k.nr_bandy = nr_bandy)

UNION ALL

SELECT      k.imie,
            k.w_stadku_od "WSTAPIL DO STADKA",
            '<--- NAJMLODSZY STAZEM W BANDZIE ' || k.nr_bandy || ' ' || b.nazwa " "
FROM        Kocury k JOIN Bandy b ON b.nr_bandy = k.nr_bandy
WHERE       k.w_stadku_od = (SELECT MAX(w_stadku_od) FROM Kocury kmax WHERE kmax.nr_bandy = k.nr_bandy)

UNION ALL

SELECT      imie,
            w_stadku_od "WSTAPIL DO STADKA",
            '<--- NAJSTARSZY STAZEM W BANDZIE ' || nazwa " "
FROM        Kocury k JOIN Bandy b ON b.nr_bandy = k.nr_bandy
WHERE       k.w_stadku_od = (SELECT MIN(w_stadku_od) FROM Kocury kmin WHERE kmin.nr_bandy = k.nr_bandy)

ORDER BY    1;

--Zadanie 31
CREATE OR REPLACE VIEW Statystyki_Bandy(nazwa, sre_spoz, max_spoz, min_spoz, koty, koty_z_dod)
AS
SELECT      nazwa,
            AVG(NVL(przydzial_myszy, 0)),
            MAX(NVL(przydzial_myszy, 0)),
            MIN(NVL(przydzial_myszy, 0)),
            COUNT(pseudo),
            COUNT(myszy_extra)
FROM        Kocury NATURAL JOIN Bandy
GROUP BY    nazwa;

SELECT * FROM Statystyki_Bandy;

SELECT      pseudo "PSEUDONIM",
            imie,
            funkcja,
            NVL(przydzial_myszy, 0) "ZJADA",
            'OD ' || min_spoz || ' DO ' || max_spoz "GRANICE SPOZYCIA",
            w_stadku_od "LOWI OD"
FROM        Kocury NATURAL JOIN Bandy NATURAL JOIN Statystyki_Bandy
WHERE       pseudo = UPPER('&pseudo');
            
--Zadanie 32
CREATE OR REPLACE VIEW Najstarsi_Stazem
AS
SELECT      pseudo,
            nr_bandy,
            plec,
            przydzial_myszy,
            myszy_extra
FROM        Kocury
WHERE       w_stadku_od IN (
                SELECT w_stadku_od FROM (
                    SELECT w_stadku_od FROM Kocury NATURAL JOIN Bandy 
                    WHERE nazwa = 'CZARNI RYCERZE'
                    ORDER BY w_stadku_od
                    FETCH NEXT 3 ROWS ONLY
                )
            )
OR          w_stadku_od IN (
                SELECT w_stadku_od FROM (
                    SELECT w_stadku_od FROM Kocury NATURAL JOIN Bandy 
                    WHERE nazwa = 'LACIACI MYSLIWI'
                    ORDER BY w_stadku_od
                    FETCH NEXT 3 ROWS ONLY
                )
            );

SELECT  pseudo,
        plec,
        NVL(przydzial_myszy, 0) "Myszy przed podw.",
        NVL(myszy_extra, 0) "Extra przed podw."
FROM    Najstarsi_Stazem;

UPDATE  Najstarsi_Stazem ns
SET     przydzial_myszy = 
            CASE WHEN plec = 'D' THEN
                przydzial_myszy + (SELECT MIN(NVL(przydzial_myszy, 0)) * 0.1 FROM Kocury)
            ELSE
                NVL(przydzial_myszy, 0) + 10
            END,
        myszy_extra = 
            NVL(myszy_extra, 0) + (SELECT AVG(NVL(myszy_extra, 0)) * 0.15 FROM Kocury WHERE ns.nr_bandy = nr_bandy);

SELECT  pseudo,
        plec,
        NVL(przydzial_myszy, 0) "Myszy po podw.",
        NVL(myszy_extra, 0) "Extra po podw."
FROM    Najstarsi_Stazem;

ROLLBACK;

--Zadanie 33
SELECT      DECODE(plec, 'Kotka', ' ', nazwa) nazwa,
            plec,
            ile,
            szefunio, 
            bandzior, 
            lowczy, 
            lapacz, 
            kot,
            milusia, 
            dzielczy, 
            suma
FROM        (
                SELECT      nazwa,
                            DECODE(plec, 'M', 'Kocor', 'Kotka') plec,
                            TO_CHAR(COUNT(pseudo)) ile,
                            TO_CHAR(SUM(DECODE(funkcja, 'SZEFUNIO', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) szefunio, 
                            TO_CHAR(SUM(DECODE(funkcja, 'BANDZIOR', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) bandzior, 
                            TO_CHAR(SUM(DECODE(funkcja, 'LOWCZY', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) lowczy, 
                            TO_CHAR(SUM(DECODE(funkcja, 'LAPACZ', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) lapacz, 
                            TO_CHAR(SUM(DECODE(funkcja, 'KOT', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) kot,
                            TO_CHAR(SUM(DECODE(funkcja, 'MILUSIA', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) milusia, 
                            TO_CHAR(SUM(DECODE(funkcja, 'DZIELCZY', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) dzielczy, 
                            TO_CHAR(SUM(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0))) suma
                FROM        Kocury NATURAL JOIN Bandy
                GROUP BY    nazwa, plec
                UNION
                SELECT 'Z--------------', '------', '--------', '---------', '---------', '--------', '--------', '--------', '--------', '--------', '--------'
                FROM DUAL
                UNION
                SELECT      'Zjada razem' nazwa,
                            ' ' plec,
                            TO_CHAR(COUNT(pseudo)) ile,
                            TO_CHAR(SUM(DECODE(funkcja, 'SZEFUNIO', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) szefunio, 
                            TO_CHAR(SUM(DECODE(funkcja, 'BANDZIOR', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) bandzior, 
                            TO_CHAR(SUM(DECODE(funkcja, 'LOWCZY', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) lowczy, 
                            TO_CHAR(SUM(DECODE(funkcja, 'LAPACZ', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) lapacz, 
                            TO_CHAR(SUM(DECODE(funkcja, 'KOT', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) kot,
                            TO_CHAR(SUM(DECODE(funkcja, 'MILUSIA', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) milusia, 
                            TO_CHAR(SUM(DECODE(funkcja, 'DZIELCZY', NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0), 0))) dzielczy, 
                            TO_CHAR(SUM(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0))) suma
                FROM        Kocury NATURAL JOIN Bandy
                ORDER BY 1, 2 
            );

SELECT      DECODE(plec, 'Kotka', ' ', nazwa) nazwa,
            plec,
            ile,
            szefunio, 
            bandzior, 
            lowczy, 
            lapacz, 
            kot,
            milusia, 
            dzielczy,
            suma
FROM        (
    (
        SELECT  TO_CHAR(nazwa) nazwa,
                DECODE(plec, 'M', 'Kocor', 'Kotka') plec,
                TO_CHAR(ile) ile,
                TO_CHAR(NVL(szefunio, 0)) szefunio, 
                TO_CHAR(NVL(bandzior, 0)) bandzior, 
                TO_CHAR(NVL(lowczy, 0)) lowczy, 
                TO_CHAR(NVL(lapacz, 0)) lapacz, 
                TO_CHAR(NVL(kot, 0)) kot, 
                TO_CHAR(NVL(milusia, 0)) milusia, 
                TO_CHAR(NVL(dzielczy, 0)) dzielczy,
                TO_CHAR(NVL(suma, 0)) suma
        FROM (
            SELECT      nazwa,
                        plec,
                        funkcja,
                        NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0) liczba
            FROM        Kocury NATURAL JOIN Bandy 
            GROUP BY    nazwa, plec, funkcja, NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)
        )
        PIVOT (
           SUM(liczba) FOR funkcja IN (
            'SZEFUNIO' szefunio, 'BANDZIOR' bandzior, 'LOWCZY' lowczy, 'LAPACZ' lapacz,
            'KOT' kot, 'MILUSIA' milusia, 'DZIELCZY' dzielczy
            )
        ) JOIN (
            SELECT      b1.nazwa n,
                        k1.plec p,
                        COUNT(pseudo) ile,
                        SUM(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) suma
            FROM        Kocury k1 INNER JOIN Bandy b1 ON b1.nr_bandy = k1.nr_bandy
            WHERE       b1.nazwa = nazwa AND k1.plec = plec
            GROUP BY    b1.nazwa, k1.plec
        ) ON n = nazwa AND p = plec
    )
    UNION
    (
        SELECT 'Z--------------', '------', '--------', '---------', '---------', '--------', '--------', '--------', '--------', '--------', '--------'
        FROM DUAL
    )
    UNION
    (
        SELECT  'Zjada razem' nazwa,
                ' ' plec,
                TO_CHAR(ile) ile,
                TO_CHAR(NVL(szefunio, 0)) szefunio, 
                TO_CHAR(NVL(bandzior, 0)) bandzior, 
                TO_CHAR(NVL(lowczy, 0)) lowczy, 
                TO_CHAR(NVL(lapacz, 0)) lapacz, 
                TO_CHAR(NVL(kot, 0)) kot, 
                TO_CHAR(NVL(milusia, 0)) milusia, 
                TO_CHAR(NVL(dzielczy, 0)) dzielczy,
                TO_CHAR(NVL(suma, 0)) suma
        FROM (
            SELECT      funkcja,
                        NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0) liczba
            FROM        Kocury NATURAL JOIN Bandy 
        )
        PIVOT (
           SUM(liczba) FOR funkcja IN (
            'SZEFUNIO' szefunio, 'BANDZIOR' bandzior, 'LOWCZY' lowczy, 'LAPACZ' lapacz,
            'KOT' kot, 'MILUSIA' milusia, 'DZIELCZY' dzielczy
            )
        ) CROSS JOIN (
            SELECT      COUNT(pseudo) ile,
                        SUM(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)) suma
            FROM        Kocury k1 INNER JOIN Bandy b1 ON b1.nr_bandy = k1.nr_bandy
        )
    )
    ORDER BY    1, 2
);

--Kartkowka

SELECT pseudo, nr_bandy
FROM Kocury
WHERE plec='D' AND nr_bandy IN (SELECT nr_bandy
                                FROM Kocury K0
                                WHERE plec='D'
                                GROUP BY nr_bandy
                                HAVING COUNT(*) < (SELECT COUNT(*)
                                                  FROM 
                                                      (SELECT DISTINCT imie_wroga
                                                      FROM Wrogowie_Kocurow WK JOIN Kocury K ON WK.pseudo=K.pseudo
                                                      WHERE K.nr_bandy = K0.nr_bandy)));
                                                      
SELECT
  pseudo,
  nr_bandy
FROM Kocury
WHERE plec = 'D' and 
  nr_bandy IN 
    (SELECT k1.nr_bandy
      FROM Kocury k1
      WHERE k1.plec = 'D'
      GROUP BY nr_bandy
      HAVING COUNT(*) < (SELECT COUNT(*)
                          FROM Kocury k2 JOIN Wrogowie_Kocurow USING(pseudo)
                          WHERE k2.nr_bandy = k1.nr_bandy));


SELECT pseudo, nr_bandy
FROM Kocury
WHERE plec='D' AND nr_bandy IN (
    SELECT k2.nr_bandy 
    FROM Kocury k2
    WHERE k2.plec='D'
    GROUP BY k2.nr_bandy
    HAVING COUNT(k2.pseudo) < (
        SELECT COUNT(DISTINCT wk.imie_wroga)
        FROM Kocury k3 JOIN Wrogowie_Kocurow wk USING(pseudo)
        WHERE plec='D' AND k2.nr_bandy = k3.nr_bandy
    )    
);

SELECT k1.pseudo, k1.nr_bandy
FROM Kocury k1 LEFT JOIN Wrogowie_Kocurow wk ON k1.pseudo = wk.pseudo
WHERE k1.plec = 'M' AND wk.imie_wroga IS NULL AND k1.nr_bandy IN (
    SELECT k2.nr_bandy
    FROM Kocury k2
    WHERE k2.plec = 'M'
    GROUP BY k2.nr_bandy
    HAVING AVG(NVL(k2.przydzial_myszy,0) + NVL(k2.myszy_extra,0)) > 55
);

SELECT * FROM (
    SELECT  k1.pseudo,
            w_stadku_od,
            nr_bandy,
            DENSE_RANK() OVER (
                ORDER BY w_stadku_od
            ) rank
    FROM    Kocury k1 JOIN Funkcje f1 ON k1.funkcja = f1.funkcja
    WHERE   NVL(k1.przydzial_myszy,0) + NVL(k1.myszy_extra,0) > (f1.min_myszy + f1.max_myszy) / 2
    AND     4 <= (SELECT COUNT(*) FROM Kocury k2 WHERE k1.nr_bandy = k2.nr_bandy)
)
WHERE rank <= 5;

SELECT pseudo, k1.nr_bandy,(SELECT COUNT(*) FROM Kocury k2 WHERE k1.nr_bandy = k2.nr_bandy) from Kocury k1 where
    4 <= (SELECT COUNT(*) FROM Kocury k2 WHERE k1.nr_bandy = k2.nr_bandy);
    
SELECT pseudo from Kocury where nr_bandy=3;

SELECT nr_bandy, COUNT(pseudo)
                    FROM Kocury
                    GROUP BY nr_bandy
                    HAVING COUNT(*)>4;

SELECT pseudo
FROM Kocury
WHERE w_stadku_od IN
    (SELECT *
    FROM
        (SELECT DISTINCT w_stadku_od
        FROM Kocury K JOIN Funkcje F ON K.funkcja=F.funkcja
        WHERE NVL(przydzial_myszy,0)>(NVL(max_myszy,0) + NVL(min_myszy,0))/2 AND nr_bandy IN 
                    (SELECT nr_bandy
                    FROM Kocury
                    GROUP BY nr_bandy
                    HAVING COUNT(*)>4)
        ORDER BY w_stadku_od)
    WHERE ROWNUM <= 1); --powinno byc 5

SELECT pseudo
FROM Kocury 
WHERE w_stadku_od IN(
SELECT *
FROM(
SELECT w_stadku_od
FROM Kocury k JOIN Funkcje f ON k.funkcja = f.funkcja
WHERE 4 < (SELECT count(*) ilosc
            FROM Kocury k2 JOIN Bandy b ON k2.nr_bandy = b.nr_bandy 
            WHERE k2.nr_bandy = k.nr_bandy)
      AND k.przydzial_myszy > ((f.min_myszy) + (f.max_myszy))/2
ORDER BY w_stadku_od asc
) 
WHERE ROWNUM <=5
);

SELECT * FROM Wrogowie_Kocurow;
SELECT * FROM Wrogowie;

SELECT nr_bandy 
FROM Kocury
GROUP BY nr_bandy
HAVING COUNT(pseudo) >=5 AND nr_bandy IN (
    SELECT nr_bandy
    FROM Kocury k1 JOIN Wrogowie_Kocurow wk ON wk.pseudo = k1.pseudo
    JOIN Wrogowie w ON w.imie_wroga = wk.imie_wroga
    WHERE w.stopien_wrogosci IN (
        SELECT stopien_wrogosci FROM (
            SELECT stopien_wrogosci, RANK() OVER (ORDER BY stopien_wrogosci DESC) rank FROM Wrogowie
        ) WHERE rank <=3
    )
    GROUP BY nr_bandy HAVING COUNT(k1.pseudo) = 2
);

SELECT K.NR_BANDY
FROM KOCURY K
WHERE K.NR_BANDY IN (
  SELECT K2.NR_BANDY
  FROM KOCURY K2
    JOIN WROGOWIE_KOCUROW WK ON K2.PSEUDO = WK.PSEUDO
    JOIN WROGOWIE W ON WK.IMIE_WROGA = W.IMIE_WROGA
  WHERE W.IMIE_WROGA IN (
    SELECT R.IMIE
    FROM (
      SELECT
        W3.IMIE_WROGA IMIE,
        W3.STOPIEN_WROGOSCI,
        RANK()        RANK
      FROM WROGOWIE W3
      ORDER BY W3.STOPIEN_WROGOSCI DESC
    ) R
    WHERE R.RANK < 3
  )
  GROUP BY K2.NR_BANDY
  HAVING COUNT(DISTINCT W.IMIE_WROGA) = 2
)
GROUP BY K.NR_BANDY
HAVING COUNT(*) > 4;

SELECT nr_bandy 
FROM Kocury
GROUP BY nr_bandy
HAVING COUNT(pseudo) >=5 AND nr_bandy IN (
    SELECT nr_bandy FROM Kocury
    NATURAL JOIN Wrogowie_Kocurow 
    WHERE imie_wroga IN (
        SELECT imie_wroga FROM Wrogowie w1
        WHERE 2 >= (
            SELECT COUNT(DISTINCT w2.imie_wroga) FROM Wrogowie w2
            WHERE w1.stopien_wrogosci > w2.stopien_wrogosci
        )
    )
    GROUP BY nr_bandy
    HAVING COUNT(pseudo) = 2
);
            


SELECT k1.pseudo, k1.nr_bandy
FROM Kocury k1 LEFT JOIN Wrogowie_Kocurow wk ON k1.pseudo = wk.pseudo
WHERE plec = 'M' 
AND wk.pseudo IS NULL
AND k1.nr_bandy IN (
    SELECT k2.nr_bandy
    FROM Kocury k2
    WHERE k2.plec = 'M'
    GROUP BY k2.nr_bandy
    HAVING AVG(k2.przydzial_myszy) > 55
);

SELECT
  k1.pseudo,
  k1.nr_bandy
FROM Kocury k1 LEFT JOIN Wrogowie_kocurow WK ON WK.pseudo = k1.pseudo
WHERE plec = 'M'
      and WK.pseudo IS NULL 
      and nr_bandy IN (SELECT nr_bandy
                        FROM Kocury 
                        WHERE plec = 'M'
                        GROUP BY nr_bandy
                        HAVING AVG(przydzial_myszy)>55);

SELECT 
  pseudo,
  nr_bandy
FROM Kocury k1
WHERE plec = 'M'
      and 55 <= (SELECT avg(przydzial_myszy) 
                FROM Kocury k2
                WHERE plec = 'M'
                GROUP BY nr_bandy
                HAVING k2.nr_bandy = k1.nr_bandy)
MINUS
SELECT 
  pseudo,
  nr_bandy
FROM Kocury JOIN Wrogowie_Kocurow USING(pseudo);

select pseudo, nr_bandy
from Kocury k1
where plec = 'D'
and (select Count(*) from Kocury k2 where plec = 'D' and k2.nr_bandy = k1.nr_bandy) < 
(select Count(*) from Kocury k3 join Wrogowie_Kocurow wk on wk.pseudo = k3.pseudo where plec = 'D' and k3.nr_bandy = k1.nr_bandy); 

select k1.pseudo, k1.nr_bandy
from Kocury k1
where k1.plec = 'D'
and k1.nr_bandy in (
    select k2.nr_bandy
    from Kocury k2
    where k2.plec = 'D'
    group by k2.nr_bandy
    having count(*) < (
        select count(*) 
        from Kocury k3 
        join Wrogowie_Kocurow wk on wk.pseudo = k3.pseudo
        where plec = 'D' and k3.nr_bandy = k2.nr_bandy
    )
);

select k1.pseudo
from Kocury k1 join Funkcje f1 on k1.funkcja = f1.funkcja
where 4 <= (select count(*) from Kocury k2 where k2.nr_bandy = k1.nr_bandy)
and k1.przydzial_myszy > (f1.max_myszy + f1.min_myszy) / 2
and 5 >= (
    select count(*) from Kocury k3 
    where 4 <= (select count(*) from Kocury k4 where k4.nr_bandy = k3.nr_bandy)
    and k3.przydzial_myszy > (f1.max_myszy + f1.min_myszy) / 2 
    and k1.w_stadku_od < k3.w_stadku_od
); 

select pseudo from (
    select  pseudo,
            dense_rank() over (
                order by w_stadku_od
            ) rank
    from Kocury k1 join Funkcje f1 on k1.funkcja = f1.funkcja
    where przydzial_myszy > (min_myszy + max_myszy) / 2
    and   4 <= (SELECT COUNT(*) FROM Kocury k2 WHERE k2.nr_bandy = k1.nr_bandy)
)
where rank <= 5;

select decode(myszy_extra, null, 1 , 0), myszy_extra from kocury;

select plec, max(przydzial_myszy), sum(decode(myszy_extra, null, 1 , 0)) 
from Kocury k1 join Funkcje f1 on k1.funkcja = f1.funkcja
where przydzial_myszy > min_myszy * 1.1
and   nr_bandy in (select nr_bandy from Kocury where pseudo in ('RAFA', 'SZYBKA'))
and   nr_bandy in (select nr_bandy from Kocury group by nr_bandy having avg(przydzial_myszy) > 50)
group by plec;


SELECT MAX(NVL(przydzial_myszy, 0)), COUNT(*) - COUNT(myszy_extra), nr_bandy, plec
FROM Kocury K2 JOIN Funkcje F ON K2.funkcja = F.funkcja
WHERE nr_bandy IN 
    (SELECT DISTINCT K.nr_bandy
    FROM Kocury K JOIN Bandy B ON K.nr_bandy = B.nr_bandy
    GROUP BY K.nr_bandy
    HAVING K.nr_bandy IN 
        (SELECT DISTINCT nr_bandy
        FROM Kocury
        WHERE pseudo IN ('RAFA', 'SZYBKA')) OR AVG(NVL(przydzial_myszy,0)) > 50)
AND NVL(przydzial_myszy, 0) > (NVL(min_myszy, 0)) * 1.1
GROUP BY nr_bandy, plec;


select * from kocury k1 join Funkcje f1 on k1.funkcja = f1.funkcja where nr_bandy in(2,4);

select nr_bandy from Kocury where pseudo in ('RAFA', 'SZYBKA');
select nr_bandy from Kocury group by nr_bandy having avg(przydzial_myszy) > 50;

select k1.pseudo, k1.nr_bandy
from Kocury k1 left join Wrogowie_Kocurow wk on k1.pseudo = wk.pseudo
where wk.imie_wroga is null
and k1.nr_bandy in (select nr_bandy from Kocury k2 group by k2.nr_bandy having avg(przydzial_myszy) > 55);
