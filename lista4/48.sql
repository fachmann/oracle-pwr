DROP TABLE Incydenty;
DROP TABLE Konta;
DROP TABLE Elita;
DROP TABLE Plebs;

CREATE TABLE Plebs
(
    nr_kota NUMBER(2)    CONSTRAINT pk_nr_kota_plebs    PRIMARY KEY,
    kot   VARCHAR(15)  CONSTRAINT rk_kot_sluga        REFERENCES Kocury (pseudo)
);

CREATE TABLE Elita
(
    nr_kota NUMBER(2)    CONSTRAINT pk_nr_kota_elita    PRIMARY KEY,
    kot   VARCHAR(15)  CONSTRAINT rk_kot_elita        REFERENCES Kocury (pseudo),
    sluga   NUMBER(2)    CONSTRAINT rk_sluga            REFERENCES Plebs (nr_kota)
);

CREATE TABLE Konta
(
    nr_myszy            NUMBER(5)   CONSTRAINT pk_nr_transakcji PRIMARY KEY,
    wlasciciel_myszy    NUMBER(2)   CONSTRAINT rk_wlasciciel    REFERENCES Elita (nr_kota),
    data_wprowadzenia   DATE,
    data_usuniecia      DATE
);

CREATE TABLE Incydenty
(
    nr_incydentu     NUMBER(5)      CONSTRAINT pk_nr_incydentu  PRIMARY KEY,
    pseudo           VARCHAR2(15)   CONSTRAINT rk_pseudo        REFERENCES Kocury (pseudo),
    imie_wroga       VARCHAR2(15)   CONSTRAINT re_imie_wroga    REFERENCES Wrogowie (imie_wroga),
    data_incydentu   DATE           NOT NULL,
    opis_incydentu   VARCHAR2(50)
);

-- Typy ------------------------------------------------------------------------

CREATE OR REPLACE TYPE KotyFlatType AS OBJECT
(
    imie VARCHAR2(15),
    plec VARCHAR2(1),
    pseudo VARCHAR2(15),
    funkcja VARCHAR2(10),
    szef VARCHAR2(15),
    w_stadku_od DATE,
    przydzial_myszy NUMBER(3),
    myszy_extra NUMBER(3),
    nr_bandy NUMBER(2),
    MEMBER FUNCTION myszy_lacznie RETURN NUMBER,
    MEMBER FUNCTION dni_w_stadku RETURN NUMBER,
    MEMBER FUNCTION daj_pseudo RETURN VARCHAR2
);
/

CREATE OR REPLACE TYPE IncydentyFlatType AS OBJECT
(
    nr_incydentu NUMBER,
    kot REF KotyFlatType,
    imie_wroga VARCHAR2(15),
    data_incydentu DATE,
    opis_incydentu VARCHAR2(50),
    MEMBER FUNCTION dane_incydentu RETURN VARCHAR2
);
/

CREATE OR REPLACE TYPE PlebsFlatType AS OBJECT
(
    nr_kota NUMBER(3),
    kot     REF KotyFlatType,
    MEMBER FUNCTION daj_pseudo_pana RETURN VARCHAR2
);
/

CREATE OR REPLACE TYPE ElitaFlatType AS OBJECT
(
    nr_kota NUMBER(3),
    kot     REF KotyFlatType,
    sluga   REF PlebsFlatType,
    MEMBER FUNCTION myszy_na_koncie RETURN NUMBER
);
/

CREATE OR REPLACE TYPE KontoFlatType AS OBJECT
(
    nr_myszy            NUMBER(5),
    kot_elita           REF ElitaFlatType,
    data_wprowadzenia   DATE,
    data_usuniecia      DATE,
    MEMBER FUNCTION czasu_na_koncie RETURN NUMBER
);
/

-- Ciala -----------------------------------------------------------------------

CREATE OR REPLACE TYPE BODY KotyFlatType AS
    MEMBER FUNCTION myszy_lacznie RETURN NUMBER IS
    BEGIN
        RETURN NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0);
    END;
    
    MEMBER FUNCTION dni_w_stadku RETURN NUMBER IS
    BEGIN
        RETURN TRUNC(SYSDATE) - w_stadku_od;
    END;
    
    MEMBER FUNCTION daj_pseudo RETURN VARCHAR2 IS
    BEGIN
        RETURN pseudo;
    END;
END;
/

CREATE OR REPLACE TYPE BODY IncydentyFlatType AS
    MEMBER FUNCTION dane_incydentu RETURN VARCHAR2 IS
    BEGIN
        RETURN  'Numer: ' || nr_incydentu ||
                ', Imie wroga: ' || imie_wroga ||
                ', Data: ' || data_incydentu ||
                ', Opis: ' || opis_incydentu;
    END;
END;
/

CREATE OR REPLACE TYPE BODY PlebsFlatType AS
    MEMBER FUNCTION daj_pseudo_pana RETURN VARCHAR2 IS
        ps_pana VARCHAR2(15);
    BEGIN
        SELECT  e.kot
        INTO    ps_pana
        FROM    Elita e
        WHERE   e.sluga = SELF.nr_kota AND ROWNUM = 1;
        RETURN  ps_pana;
    EXCEPTION 
        WHEN NO_DATA_FOUND THEN RETURN 'brak pana';
    END;
END;
/

CREATE OR REPLACE TYPE BODY ElitaFlatType AS
    MEMBER FUNCTION myszy_na_koncie RETURN NUMBER IS
        ile NUMBER;
    BEGIN
        SELECT  COUNT(*)
        INTO    ile
        FROM    Konta
        WHERE   wlasciciel_myszy = nr_kota;
        RETURN  ile;
    END;
END;
/

CREATE OR REPLACE TYPE BODY KontoFlatType AS
    MEMBER FUNCTION czasu_na_koncie RETURN NUMBER IS
    BEGIN
        IF data_usuniecia IS NULL THEN
            RETURN SYSDATE - data_wprowadzenia;
        ELSE
            RETURN data_usuniecia - data_wprowadzenia;
        END IF;
    END;
END;
/

-- Perspektywy -----------------------------------------------------------------

CREATE OR REPLACE VIEW KocuryNakl OF KotyFlatType WITH OBJECT IDENTIFIER (pseudo) AS
SELECT  imie,
        plec,
        pseudo,
        funkcja,
        szef,
        w_stadku_od,
        przydzial_myszy,
        myszy_extra,
        nr_bandy
FROM    Kocury;

CREATE OR REPLACE VIEW IncydentyNakl OF IncydentyFlatType WITH OBJECT IDENTIFIER (nr_incydentu) AS 
SELECT  nr_incydentu,
        MAKE_REF(KocuryNakl, pseudo) kot,
        imie_wroga,
        data_incydentu,
        opis_incydentu 
FROM    Incydenty;

CREATE OR REPLACE VIEW PlebsNakl OF PlebsFlatType WITH OBJECT IDENTIFIER (nr_kota) AS
SELECT  nr_kota,
        MAKE_REF(KocuryNakl, kot) kot
FROM    Plebs;

CREATE OR REPLACE VIEW ElitaNakl OF ElitaFlatType WITH OBJECT IDENTIFIER (nr_kota) AS
SELECT  nr_kota,
        MAKE_REF(KocuryNakl, kot) kot,
        MAKE_REF(PlebsNakl, sluga) sluga 
FROM    Elita;

CREATE OR REPLACE VIEW KontaNakl OF KontoFlatType WITH OBJECT IDENTIFIER (nr_myszy) AS
SELECT  nr_myszy,
        MAKE_REF(ElitaNakl, wlasciciel_myszy) kot_elita,
        data_wprowadzenia,
        data_usuniecia
FROM    Konta;


-- Uzupelnienie nowych encji ---------------------------------------------------

INSERT INTO Plebs
    SELECT ROWNUM, pseudo FROM KocuryNakl k WHERE k.myszy_lacznie() < 60;

INSERT INTO Elita
    SELECT      ROWNUM, 
                k.pseudo,
                ROWNUM
    FROM        KocuryNakl k 
    WHERE       k.myszy_lacznie() > 70;

INSERT ALL
    INTO Konta VALUES (1, 1, SYSDATE, NULL)
    INTO Konta VALUES (4, 1, SYSDATE, NULL)
    INTO Konta VALUES (2, 2, SYSDATE, NULL)
    INTO Konta VALUES (3, 3, SYSDATE, NULL)
SELECT * FROM Dual;

INSERT INTO Incydenty
SELECT  Nr_Incydentu,
        i.kot.Pseudo,
        Imie_Wroga,
        Data_Incydentu,
        Opis_Incydentu
FROM IncydentyOrd i;

SELECT * FROM Plebs;
SELECT * FROM Elita;
SELECT * FROM Konta;
SELECT * FROM Incydenty;

--DELETE FROM Elita;
--DELETE FROM Plebs;
--DELETE FROM Konta;
--DELETE FROM Incydenty;

Commit;


-- Zadania ---------------------------------------------------------------------

SELECT k.pseudo, k.myszy_lacznie() FROM KocuryNakl k;

SELECT      MIN(b.nazwa) "Nazwa bandy",
            SUM(k.myszy_lacznie()) "Zjada w bandzie"
FROM        KocuryNakl k
JOIN        Bandy b ON b.nr_bandy = k.nr_bandy
GROUP BY    k.nr_bandy;

SELECT  e.kot.pseudo "Kot z elity",
        i.dane_incydentu() "Incydenty kota"
FROM    ElitaNakl e
JOIN    IncydentyNakl i ON E.kot = I.kot;

SELECT  b.*,
        ROUND(przydzial_szefa / przydzial_bandy * 100, 2) "Procentowy udzial szefa"
FROM    (
            SELECT  b.*,
                    (SELECT k.myszy_lacznie() 
                     FROM   KocuryNakl k 
                     WHERE k.pseudo = b.szef_bandy) przydzial_szefa,
                    (SELECT SUM(k.myszy_lacznie())
                     FROM   KocuryNakl k
                     WHERE  k.nr_bandy = b.nr_bandy) przydzial_bandy
            FROM    Bandy b
        ) b;

SELECT  e.kot.pseudo "Psuedo kota",
        e.myszy_na_koncie() "Ile myszy na koncie"
FROM    ElitaNakl e;


SELECT  nr_kota, 
        e.kot.pseudo,
        e.sluga.daj_pseudo_pana(),
        e.sluga.kot.pseudo
FROM    ElitaOrd e;

-- Zadania z list --------------------------------------------------------------

-- Zadanie 18
SELECT k1.imie, k1.w_stadku_od "Poluje od"
FROM KocuryNakl k1 
JOIN KocuryNakl k2 ON k2.imie='JACEK' AND k1.w_stadku_od < k2.w_stadku_od
ORDER BY k1.w_stadku_od DESC;

--Zadanie 23
SELECT      imie,
            k.myszy_lacznie() * 12 "DAWKA ROCZNA",
            'Ponizej 864'
FROM        KocuryNakl k
WHERE       k.myszy_lacznie() * 12 < 864
UNION
SELECT      imie,
            k.myszy_lacznie() * 12 "DAWKA ROCZNA",
            '864'
FROM        KocuryNakl k
WHERE       k.myszy_lacznie() * 12 = 864
UNION
SELECT      imie,
            k.myszy_lacznie() * 12 "DAWKA ROCZNA",
            'Powyzej 864'
FROM        KocuryNakl k
WHERE       k.myszy_lacznie() * 12 > 864
ORDER BY    "DAWKA ROCZNA" DESC;

-- Zadanie 34
DECLARE
    kocury_f VARCHAR2(255);
BEGIN 
    SELECT      MIN(funkcja)
    INTO        kocury_f
    FROM        KocuryNakl
    WHERE       funkcja = '&funkcja'
    GROUP BY    funkcja;
    DBMS_OUTPUT.PUT_LINE(kocury_f);
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota o podanej funkcji !');
END;
/

-- Zadanie 37
DECLARE
    i NUMBER := 0;
BEGIN
    DBMS_OUTPUT.PUT_LINE('Nr   Pseudonim   Zjada ');
    DBMS_OUTPUT.PUT_LINE('-----------------------');
    FOR elem IN (SELECT     pseudo, 
                            k.myszy_lacznie() zjada 
                 FROM       KocuryNakl k 
                 ORDER BY   2 DESC)
    LOOP
        i := i + 1;
        DBMS_OUTPUT.PUT_LINE(
            i || '    ' || 
            RPAD(elem.pseudo,9, ' ') || '   ' || 
            elem.zjada
        );
        EXIT WHEN i = 5;
    END LOOP;
END;
/


