DROP TABLE KotyOrd;
DROP TABLE IncydentyOrd;
DROP TABLE PlebsOrd;
DROP TABLE ElitaOrd;
DROP TABLE KontoOrd;
DROP TYPE KotyType;
DROP TYPE IncydentyType;
DROP TYPE KontoType;
DROP TYPE ElitaType;
DROP TYPE PlebsType;

CREATE OR REPLACE TYPE KotyType AS OBJECT
(
    imie VARCHAR2(15),
    plec VARCHAR2(1),
    pseudo VARCHAR2(15),
    funkcja VARCHAR2(10),
    szef REF KotyType,
    w_stadku_od DATE,
    przydzial_myszy NUMBER(3),
    myszy_extra NUMBER(3),
    nr_bandy NUMBER(2),
    MEMBER FUNCTION myszy_lacznie RETURN NUMBER,
    MEMBER FUNCTION dni_w_stadku RETURN NUMBER,
    MEMBER FUNCTION daj_pseudo RETURN VARCHAR2
);
/

CREATE TABLE KotyOrd OF KotyType
(
    CONSTRAINT PS_KOT_PK PRIMARY KEY (pseudo) 
);

CREATE OR REPLACE TYPE BODY KotyType AS
    MEMBER FUNCTION myszy_lacznie RETURN NUMBER IS
    BEGIN
        RETURN NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0);
    END;
    
    MEMBER FUNCTION dni_w_stadku RETURN NUMBER IS
    BEGIN
        RETURN TRUNC(SYSDATE) - w_stadku_od;
    END;
    
    MEMBER FUNCTION daj_pseudo RETURN VARCHAR2 IS
    BEGIN
        RETURN pseudo;
    END;
END;
/

--INSERT ALL
--    INTO KotyOrd VALUES ('JACEK', 'M', 'PLACEK', 'LOWCZY', 'LYSY', '2008-12-01', 67, NULL, 2)
--    INTO KotyOrd VALUES ('BARI', 'M', 'RURA', 'LAPACZ', 'LYSY', '2009-09-01', 56, NULL, 2)
--    INTO KotyOrd VALUES ('MICKA', 'D', 'LOLA', 'MILUSIA', 'TYGRYS', '2009-10-14', 25, 47, 1)
--    INTO KotyOrd VALUES ('LUCEK', 'M', 'ZERO', 'KOT', 'KURKA', '2010-03-01', 43, NULL, 3)
--    INTO KotyOrd VALUES ('SONIA', 'D', 'PUSZYSTA', 'MILUSIA', 'ZOMBI', '2010-11-18', 20, 35, 3)
--    INTO KotyOrd VALUES ('LATKA', 'D', 'UCHO', 'KOT', 'RAFA', '2011-01-01', 40, NULL, 4)
--    INTO KotyOrd VALUES ('DUDEK', 'M', 'MALY', 'KOT', 'RAFA', '2011-05-15', 40, NULL, 4)
--    INTO KotyOrd VALUES ('MRUCZEK', 'M', 'TYGRYS', 'SZEFUNIO' ,NULL, '2002-01-01', 103, 33, 1)
--    INTO KotyOrd VALUES ('CHYTRY', 'M', 'BOLEK', 'DZIELCZY', 'TYGRYS', '2002-05-05', 50, NULL, 1)
--    INTO KotyOrd VALUES ('KOREK', 'M', 'ZOMBI', 'BANDZIOR', 'TYGRYS', '2004-03-16', 75, 13, 3)
--    INTO KotyOrd VALUES ('BOLEK', 'M', 'LYSY', 'BANDZIOR', 'TYGRYS', '2006-08-15', 72, 21, 2)
--    INTO KotyOrd VALUES ('ZUZIA', 'D', 'SZYBKA', 'LOWCZY', 'LYSY', '2006-07-21', 65, NULL, 2)
--    INTO KotyOrd VALUES ('RUDA', 'D', 'MALA', 'MILUSIA', 'TYGRYS', '2006-09-17', 22, 42, 1)
--    INTO KotyOrd VALUES ('PUCEK', 'M', 'RAFA', 'LOWCZY', 'TYGRYS', '2006-10-15', 65, NULL, 4)
--    INTO KotyOrd VALUES ('PUNIA', 'D', 'KURKA', 'LOWCZY', 'ZOMBI', '2008-01-01', 61, NULL, 3)
--    INTO KotyOrd VALUES ('BELA', 'D', 'LASKA', 'MILUSIA', 'LYSY', '2008-02-01', 24, 28, 2)
--    INTO KotyOrd VALUES ('KSAWERY', 'M', 'MAN', 'LAPACZ', 'RAFA', '2008-07-12', 51, NULL, 4)
--    INTO KotyOrd VALUES ('MELA', 'D', 'DAMA', 'LAPACZ', 'RAFA', '2008-11-01', 51, NULL, 4)
--SELECT * FROM dual;

INSERT INTO KotyOrd
    SELECT 'MRUCZEK', 'M', 'TYGRYS', 'SZEFUNIO', NULL, '2002-01-01', 103, 33, 1 FROM Dual;

INSERT INTO KotyOrd
    SELECT 'CHYTRY', 'M', 'BOLEK', 'DZIELCZY', REF(k), '2002-05-05', 50, NULL, 1 FROM KotyOrd k WHERE k.pseudo = 'TYGRYS' UNION
    SELECT 'KOREK', 'M', 'ZOMBI', 'BANDZIOR', REF(k), '2004-03-16', 75, 13, 3 FROM KotyOrd k WHERE k.pseudo = 'TYGRYS' UNION
    SELECT 'BOLEK', 'M', 'LYSY', 'BANDZIOR', REF(k), '2006-08-15', 72, 21, 2 FROM KotyOrd k WHERE k.pseudo = 'TYGRYS' UNION
    SELECT 'RUDA', 'D', 'MALA', 'MILUSIA', REF(k), '2006-09-17', 22, 42, 1 FROM KotyOrd k WHERE k.pseudo = 'TYGRYS' UNION
    SELECT 'PUCEK', 'M', 'RAFA', 'LOWCZY', REF(k), '2006-10-15', 65, NULL, 4 FROM KotyOrd k WHERE k.pseudo = 'TYGRYS' UNION
    SELECT 'MICKA', 'D', 'LOLA', 'MILUSIA', REF(k), '2009-10-14', 25, 47, 1 FROM KotyOrd k WHERE k.pseudo = 'TYGRYS';

INSERT INTO KotyOrd
    SELECT 'SONIA', 'D', 'PUSZYSTA', 'MILUSIA', REF(k), '2010-11-18', 20, 35, 3 FROM KotyOrd k WHERE k.pseudo = 'ZOMBI' UNION
    SELECT 'PUNIA', 'D', 'KURKA', 'LOWCZY', REF(k), '2008-01-01', 61, NULL, 3 FROM KotyOrd k WHERE k.pseudo = 'ZOMBI' UNION
    SELECT 'JACEK', 'M', 'PLACEK', 'LOWCZY', REF(k), '2008-12-01', 67, NULL, 2 FROM KotyOrd k WHERE k.pseudo = 'LYSY' UNION
    SELECT 'BARI', 'M', 'RURA', 'LAPACZ', REF(k), '2009-09-01', 56, NULL, 2 FROM KotyOrd k WHERE k.pseudo = 'LYSY' UNION
    SELECT 'ZUZIA', 'D', 'SZYBKA', 'LOWCZY', REF(k), '2006-07-21', 65, NULL, 2 FROM KotyOrd k WHERE k.pseudo = 'LYSY' UNION
    SELECT 'BELA', 'D', 'LASKA', 'MILUSIA', REF(k), '2008-02-01', 24, 28, 2 FROM KotyOrd k WHERE k.pseudo = 'LYSY' UNION
    SELECT 'LATKA', 'D', 'UCHO', 'KOT', REF(k), '2011-01-01', 40, NULL, 4 FROM KotyOrd k WHERE k.pseudo = 'RAFA' UNION
    SELECT 'DUDEK', 'M', 'MALY', 'KOT', REF(k), '2011-05-15', 40, NULL, 4 FROM KotyOrd k WHERE k.pseudo = 'RAFA' UNION
    SELECT 'KSAWERY', 'M', 'MAN', 'LAPACZ', REF(k), '2008-07-12', 51, NULL, 4 FROM KotyOrd k WHERE k.pseudo = 'RAFA' UNION
    SELECT 'MELA', 'D', 'DAMA', 'LAPACZ', REF(k), '2008-11-01', 51, NULL, 4 FROM KotyOrd k WHERE k.pseudo = 'RAFA';

INSERT INTO KotyOrd
    SELECT 'LUCEK', 'M', 'ZERO', 'KOT', REF(k), '2010-03-01', 43, NULL, 3 FROM KotyOrd k WHERE k.pseudo = 'KURKA';



--DELETE FROM KotyOrd;

SELECT k.dni_w_stadku() FROM KotyOrd k WHERE k.pseudo = 'PLACEK';
SELECT k.daj_pseudo() FROM KotyOrd k WHERE k.pseudo = 'PLACEK';
SELECT k.myszy_lacznie() FROM KotyOrd k WHERE k.daj_pseudo() = 'TYGRYS';
SELECT VALUE(k).imie FROM KotyOrd k;
SELECT imie FROM KotyOrd k;
SELECT k.pseudo, k.daj_pseudo(), k.szef, k.szef.pseudo FROM KotyOrd k;

CREATE OR REPLACE TYPE IncydentyType AS OBJECT
(
    nr_incydentu NUMBER,
    kot REF KotyType,
    imie_wroga VARCHAR2(15),
    data_incydentu DATE,
    opis_incydentu VARCHAR2(50),
    MEMBER FUNCTION dane_incydentu RETURN VARCHAR2
);
/

CREATE OR REPLACE TYPE BODY IncydentyType AS
    MEMBER FUNCTION dane_incydentu RETURN VARCHAR2 IS
    BEGIN
        RETURN  'Numer: ' || nr_incydentu ||
                ', Imie wroga: ' || imie_wroga ||
                ', Data: ' || data_incydentu ||
                ', Opis: ' || opis_incydentu;
    END;
END;
/

CREATE TABLE IncydentyOrd OF IncydentyType
(
    kot SCOPE IS KotyOrd CONSTRAINT i_kot_nn NOT NULL,
    imie_wroga CONSTRAINT i_imie_wroga_nn NOT NULL,
    CONSTRAINT in_zlozony_pk PRIMARY KEY (nr_incydentu)
);

INSERT INTO IncydentyOrd
    SELECT 1,REF(K),'KAZIO','2004-10-13','USILOWAL NABIC NA WIDLY' FROM KotyOrd K WHERE K.pseudo='TYGRYS' UNION
    SELECT 2,REF(K),'SWAWOLNY DYZIO','2005-03-07','WYBIL OKO Z PROCY' FROM KotyOrd K WHERE K.pseudo='ZOMBI' UNION
    SELECT 3,REF(K),'KAZIO','2005-03-29','POSZCZUL BURKIEM' FROM KotyOrd K WHERE K.pseudo='BOLEK' UNION
    SELECT 4,REF(K),'GLUPIA ZOSKA','2006-09-12','UZYLA KOTA JAKO SCIERKI' FROM KotyOrd K WHERE K.pseudo='SZYBKA' UNION
    SELECT 5,REF(K),'CHYTRUSEK','2007-03-07','ZALECAL SIE' FROM KotyOrd K WHERE K.pseudo='MALA' UNION
    SELECT 6,REF(K),'DZIKI BILL','2007-06-12','USILOWAL POZBAWIC ZYCIA' FROM KotyOrd K WHERE K.pseudo='TYGRYS' UNION
    SELECT 7,REF(K),'DZIKI BILL','2007-11-10','ODGRYZL UCHO' FROM KotyOrd K WHERE K.pseudo='BOLEK' UNION
    SELECT 8,REF(K),'DZIKI BILL','2008-12-12','POGRYZL ZE LEDWO SIE WYLIZALA' FROM KotyOrd K WHERE K.pseudo='LASKA' UNION
    SELECT 9,REF(K),'KAZIO','2009-01-07','ZLAPAL ZA OGON I ZROBIL WIATRAK' FROM KotyOrd K WHERE K.pseudo='LASKA' UNION
    SELECT 10,REF(K),'KAZIO','2009-02-07','CHCIAL OBEDRZEC ZE SKORY' FROM KotyOrd K WHERE K.pseudo='DAMA' UNION
    SELECT 11,REF(K),'REKSIO','2009-04-14','WYJATKOWO NIEGRZECZNIE OBSZCZEKAL' FROM KotyOrd K WHERE K.pseudo='MAN' UNION
    SELECT 12,REF(K),'BETHOVEN','2009-05-11','NIE PODZIELIL SIE SWOJA KASZA'FROM KotyOrd K WHERE K.pseudo='LYSY' UNION
    SELECT 13,REF(K),'DZIKI BILL','2009-09-03','ODGRYZL OGON' FROM KotyOrd K WHERE K.pseudo='RURA' UNION
    SELECT 14,REF(K),'BAZYLI','2010-07-12','DZIOBIAC UNIEMOZLIWIL PODEBRANIE KURCZAKA' FROM KotyOrd K WHERE K.pseudo='PLACEK' UNION
    SELECT 15,REF(K),'SMUKLA','2010-11-19','OBRZUCILA SZYSZKAMI' FROM KotyOrd K WHERE K.pseudo='PUSZYSTA' UNION
    SELECT 16,REF(K),'BUREK','2010-12-14','POGONIL' FROM KotyOrd K WHERE K.pseudo='KURKA' UNION
    SELECT 17,REF(K),'CHYTRUSEK','2011-07-13','PODEBRAL PODEBRANE JAJKA' FROM KotyOrd K WHERE K.pseudo='MALY' UNION
    SELECT 18,REF(K),'SWAWOLNY DYZIO','2011-07-14','OBRZUCIL KAMIENIAMI' FROM KotyOrd K WHERE K.pseudo='UCHO';

SELECT * FROM IncydentyOrd;

-- Typy ------------------------------------------------------------------------

CREATE OR REPLACE TYPE PlebsType AS OBJECT
(
    nr_kota NUMBER(3),
    kot     REF KotyType,
    MEMBER FUNCTION daj_pseudo_pana RETURN VARCHAR2
);
/

CREATE OR REPLACE TYPE ElitaType AS OBJECT
(
    nr_kota NUMBER(3),
    kot     REF KotyType,
    sluga   REF PlebsType,
    MEMBER FUNCTION myszy_na_koncie RETURN NUMBER
);
/

CREATE OR REPLACE TYPE KontoType AS OBJECT
(
    nr_myszy            NUMBER(5),
    kot_elita           REF ElitaType,
    data_wprowadzenia   DATE,
    data_usuniecia      DATE,
    MEMBER FUNCTION czasu_na_koncie RETURN NUMBER
);
/

-- Tabele ----------------------------------------------------------------------

CREATE TABLE PlebsOrd OF PlebsType
(
    kot SCOPE IS KotyOrd CONSTRAINT pl_kot_nn NOT NULL,
    CONSTRAINT nr_kot_pk PRIMARY KEY (nr_kota) 
);

CREATE TABLE ElitaOrd OF ElitaType
(
    kot SCOPE IS KotyOrd CONSTRAINT el_kot_nn NOT NULL,
    sluga SCOPE IS PlebsOrd CONSTRAINT el_sluga_nn NOT NULL,
    CONSTRAINT nr_kota_e_pk PRIMARY KEY (nr_kota) 
);

CREATE TABLE KontoOrd OF KontoType
(
    kot_elita SCOPE IS ElitaOrd CONSTRAINT ko_kot_e_nn NOT NULL,
    data_wprowadzenia DEFAULT SYSDATE,
    CONSTRAINT nr_konto_ord_pk PRIMARY KEY (nr_myszy)  
);

-- Ciala -----------------------------------------------------------------------

CREATE OR REPLACE TYPE BODY PlebsType AS
    MEMBER FUNCTION daj_pseudo_pana RETURN VARCHAR2 IS
        ps_pana VARCHAR2(15);
    BEGIN
        SELECT  e.kot.pseudo 
        INTO    ps_pana
        FROM    ElitaOrd e
        WHERE   DEREF(e.sluga) = SELF;
        RETURN  ps_pana;
    EXCEPTION 
        WHEN NO_DATA_FOUND THEN RETURN 'brak pana';
    END;
END;
/
 
CREATE OR REPLACE TYPE BODY ElitaType AS
    MEMBER FUNCTION myszy_na_koncie RETURN NUMBER IS
        ile NUMBER;
    BEGIN
        SELECT  COUNT(*)
        INTO    ile
        FROM    KontoOrd K
        WHERE   DEREF(K.kot_elita) = SELF;
        RETURN  ile;
    END;
END;
/
 
CREATE OR REPLACE TYPE BODY KontoType AS
    MEMBER FUNCTION czasu_na_koncie RETURN NUMBER IS
    BEGIN
        IF data_usuniecia IS NULL THEN
            RETURN SYSDATE - data_wprowadzenia;
        ELSE
            RETURN data_usuniecia - data_wprowadzenia;
        END IF;
    END;
END;
/

-- Uzupelnienie nowych encji ---------------------------------------------------

INSERT INTO PlebsOrd
    SELECT ROWNUM, REF(k) FROM  KotyOrd k WHERE k.myszy_lacznie() < 60;

INSERT INTO ElitaOrd
SELECT      ROWNUM, 
            REF(k), 
            REF(p)
FROM        KotyOrd k 
LEFT JOIN   PlebsOrd p ON p.nr_kota = ROWNUM
WHERE       k.myszy_lacznie() > 70;

SELECT nr_kota, p.kot.pseudo FROM PlebsOrd p;

SELECT  nr_kota, 
        e.kot.pseudo,
        e.sluga.daj_pseudo_pana(),
        e.sluga.kot.pseudo
FROM    ElitaOrd e;

--DELETE FROM PlebsOrd;
--DELETE FROM ElitaOrd;

--DESC KontoOrd;

INSERT INTO KontoOrd
    SELECT 1, REF(e), SYSDATE, NULL FROM ElitaOrd e WHERE e.nr_kota = 1 UNION
    SELECT 4, REF(e), SYSDATE, NULL FROM ElitaOrd e WHERE e.nr_kota = 1 UNION
    SELECT 2, REF(e), SYSDATE, NULL FROM ElitaOrd e WHERE e.nr_kota = 2 UNION
    SELECT 3, REF(e), SYSDATE, NULL FROM ElitaOrd e WHERE e.nr_kota = 3;

SELECT * FROM KontoOrd;

-- Przykladowe zapytania -------------------------------------------------------

SELECT      MIN(b.nazwa) "Nazwa bandy",
            SUM(k.myszy_lacznie()) "Zjada w bandzie"
FROM        KotyOrd k
JOIN        Bandy b ON b.nr_bandy = k.nr_bandy
GROUP BY    k.nr_bandy;

SELECT  e.kot.pseudo "Kot z elity",
        i.dane_incydentu() "Incydenty kota"
FROM    ElitaOrd e
JOIN    IncydentyOrd i ON E.kot = I.kot;

SELECT  b.*,
        ROUND(przydzial_szefa / przydzial_bandy * 100, 2) "Procentowy udzial szefa"
FROM    (
            SELECT  b.*,
                    (SELECT k.myszy_lacznie() 
                     FROM   KotyOrd k 
                     WHERE k.pseudo = b.szef_bandy) przydzial_szefa,
                    (SELECT SUM(k.myszy_lacznie())
                     FROM   KotyOrd k
                     WHERE  k.nr_bandy = b.nr_bandy) przydzial_bandy
            FROM    Bandy b
        ) b;

SELECT  e.kot.pseudo "Psuedo kota",
        e.myszy_na_koncie() "Ile myszy na koncie"
FROM    ElitaOrd e;


SELECT  nr_kota, 
        e.kot.pseudo,
        e.sluga.daj_pseudo_pana(),
        e.sluga.kot.pseudo
FROM    ElitaOrd e;

-- Zadania z list --------------------------------------------------------------

-- Zadanie 18
SELECT k1.imie, k1.w_stadku_od "Poluje od"
FROM KotyOrd k1 
JOIN KotyOrd k2 ON k2.imie='JACEK' AND k1.w_stadku_od < k2.w_stadku_od
ORDER BY k1.w_stadku_od DESC;

-- Zadanie 19
SELECT  k.imie,
        k.funkcja,
        NVL(k.szef.pseudo, ' ')  "Szef 1",
        NVL(k.szef.szef.pseudo, ' ') "Szef 2",
        NVL(k.szef.szef.szef.pseudo, ' ') "Szef 3"
FROM    KotyOrd k
WHERE   k.funkcja IN ('KOT', 'MILUSIA');

-- Zadanie 34

DECLARE
    kocury_f VARCHAR2(255);
BEGIN 
    SELECT      MIN(funkcja)
    INTO        kocury_f
    FROM        KotyOrd
    WHERE       funkcja = '&funkcja'
    GROUP BY    funkcja;
    DBMS_OUTPUT.PUT_LINE(kocury_f);
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota o podanej funkcji !');
END;
/

-- Zadanie 37
DECLARE
    i NUMBER := 0;
BEGIN
    DBMS_OUTPUT.PUT_LINE('Nr   Pseudonim   Zjada ');
    DBMS_OUTPUT.PUT_LINE('-----------------------');
    FOR elem IN (SELECT     pseudo, 
                            k.myszy_lacznie() zjada 
                 FROM       KotyOrd k 
                 ORDER BY   2 DESC)
    LOOP
        i := i + 1;
        DBMS_OUTPUT.PUT_LINE(
            i || '    ' || 
            RPAD(elem.pseudo,9, ' ') || '   ' || 
            elem.zjada
        );
        EXIT WHEN i = 5;
    END LOOP;
END;
/
