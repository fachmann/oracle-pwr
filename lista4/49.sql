--Zadanie 49

-- Zalozenie tabeli ------------------------------------------------------------

DROP TABLE Myszy;

DECLARE BEGIN
    DECLARE BEGIN
       EXECUTE IMMEDIATE 'DROP TABLE Myszy';
    EXCEPTION
        WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
    END;
    EXECUTE IMMEDIATE 'CREATE TABLE Myszy ( nr_myszy NUMBER CONSTRAINT myszy_nr_myszy_pk PRIMARY KEY, lowca VARCHAR2(15) CONSTRAINT myszy_lowca_fk REFERENCES Kocury(pseudo), zjadacz VARCHAR2(15) CONSTRAINT myszy_zjadacz_fk REFERENCES Kocury(pseudo), waga_myszy NUMBER CONSTRAINT myszy_waga_myszy_ck CHECK (waga_myszy BETWEEN 16 and 60), data_zlowienia DATE CONSTRAINT myszy_dz_nn NOT NULL, data_wydania DATE CONSTRAINT myszy_data_wydania_ck CHECK(data_wydania = (NEXT_DAY(LAST_DAY(data_wydania)-7, ''Wednesday''))), CONSTRAINT myszy_daty_ck CHECK (data_zlowienia <= data_wydania) )';
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/

SELECT NEXT_DAY(LAST_DAY(SYSDATE)-7, 5) FROM Dual;
SELECT * FROM Myszy;

-- Wypenienie danymi -----------------------------------------------------------

DECLARE 
    TYPE PseudoMyszyPair IS RECORD (pseudo Kocury.pseudo%TYPE, myszy_lacznie Kocury.przydzial_myszy%TYPE);
    TYPE PseudoMyszyPairTable IS TABLE OF PseudoMyszyPair INDEX BY BINARY_INTEGER;
    lista_kotow     PseudoMyszyPairTable;
    zjadacz_indeks  BINARY_INTEGER;

    TYPE MyszyTable IS TABLE OF Myszy%ROWTYPE INDEX BY BINARY_INTEGER;  
    lista_myszy MyszyTable;
    myszy_indeks BINARY_INTEGER:=1;

    data_start      DATE := TO_DATE('2004-01-01', 'YYYY-MM-DD');
    data_koniec     DATE := TO_DATE('2018-01-09', 'YYYY-MM-DD');
    ostatnia_sroda  DATE;
    data_zlowienia  Myszy.data_zlowienia%TYPE;

    srednia_liczba_myszy    NUMBER;
    kot_zjadl               NUMBER;
BEGIN
    DELETE FROM Myszy;
    WHILE data_start <= data_koniec LOOP
        SELECT  pseudo, 
                k.myszy_lacznie() BULK COLLECT INTO lista_kotow
        FROM    KocuryNakl k
        WHERE   w_stadku_od < data_start;
        
        SELECT  CEIL(AVG(k.myszy_lacznie()))
        INTO    srednia_liczba_myszy
        FROM    KocuryNakl k
        WHERE   w_stadku_od < data_start;
        
        SELECT NEXT_DAY(LAST_DAY(data_start)-7, 3) INTO ostatnia_sroda FROM  Dual;
        zjadacz_indeks := 1;
        kot_zjadl := 0;
        
        DBMS_OUTPUT.PUT_LINE(data_start || ' - ' || ostatnia_sroda || ' : ' || lista_kotow.COUNT);
        FOR i IN 1 .. lista_kotow.COUNT LOOP
            FOR j IN 1..srednia_liczba_myszy LOOP
                data_zlowienia := data_start + DBMS_RANDOM.VALUE(0, ostatnia_sroda - data_start);

                IF data_zlowienia <= data_koniec THEN
                    lista_myszy(myszy_indeks).nr_myszy        := myszy_indeks;
                    lista_myszy(myszy_indeks).zjadacz         := lista_kotow(zjadacz_indeks).pseudo;
                    lista_myszy(myszy_indeks).lowca           := lista_kotow(i).pseudo;
                    lista_myszy(myszy_indeks).waga_myszy      := ROUND(DBMS_RANDOM.VALUE(16, 60), 2);
                    lista_myszy(myszy_indeks).data_zlowienia  := data_zlowienia;
                    lista_myszy(myszy_indeks).data_wydania    := ostatnia_sroda;
                    
                    myszy_indeks := myszy_indeks + 1;
                    kot_zjadl := kot_zjadl + 1;
                    
                    IF kot_zjadl = lista_kotow(zjadacz_indeks).myszy_lacznie THEN
                        kot_zjadl := 0;
                        zjadacz_indeks := MOD(zjadacz_indeks, lista_kotow.COUNT) + 1;
                    END IF;
                END IF;
            END LOOP;
        END LOOP;
        data_start := ADD_MONTHS(data_start, 1);
    END LOOP;
    
    FORALL i IN 1 .. lista_myszy.COUNT
        INSERT INTO Myszy VALUES (
            lista_myszy(i).nr_myszy,
            lista_myszy(i).lowca,
            lista_myszy(i).zjadacz,
            lista_myszy(i).waga_myszy,
            lista_myszy(i).data_zlowienia,
            lista_myszy(i).data_wydania
        );
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/

SELECT zjadacz, count(nr_myszy), (select k.myszy_lacznie() FROM KocuryNakl k where k.pseudo = zjadacz) FROM Myszy where data_wydania = '17/02/22' group by zjadacz;
SELECT lowca, count(nr_myszy) FROM Myszy where data_wydania = '17/02/22' group by lowca;
SELECT * FROM Myszy where data_wydania = '17/02/22';
DELETE FROM Myszy;
SELECT COUNT(*) FROM Myszy;
SELECT * FROM Myszy;

-- Tabele indywidualne ---------------------------------------------------------

SELECT 'DROP TABLE ZLOWIONE_MYSZY_' || pseudo || ';' FROM Kocury;

DROP TABLE ZLOWIONE_MYSZY_BOLEK;
DROP TABLE ZLOWIONE_MYSZY_DAMA;
DROP TABLE ZLOWIONE_MYSZY_KURKA;
DROP TABLE ZLOWIONE_MYSZY_LASKA;
DROP TABLE ZLOWIONE_MYSZY_LOLA;
DROP TABLE ZLOWIONE_MYSZY_LYSY;
DROP TABLE ZLOWIONE_MYSZY_MALA;
DROP TABLE ZLOWIONE_MYSZY_MALY;
DROP TABLE ZLOWIONE_MYSZY_MAN;
DROP TABLE ZLOWIONE_MYSZY_PLACEK;
DROP TABLE ZLOWIONE_MYSZY_PUSZYSTA;
DROP TABLE ZLOWIONE_MYSZY_RAFA;
DROP TABLE ZLOWIONE_MYSZY_RURA;
DROP TABLE ZLOWIONE_MYSZY_SZYBKA;
DROP TABLE ZLOWIONE_MYSZY_TYGRYS;
DROP TABLE ZLOWIONE_MYSZY_UCHO;
DROP TABLE ZLOWIONE_MYSZY_ZERO;
DROP TABLE ZLOWIONE_MYSZY_ZOMBI;

DECLARE BEGIN
    FOR KOT IN (SELECT PSEUDO FROM KOCURY) LOOP
        EXECUTE IMMEDIATE 'CREATE TABLE ZLOWIONE_MYSZY_' || KOT.PSEUDO ||'(
        nr_myszy INTEGER CONSTRAINT pk_nr_myszy_' || KOT.PSEUDO || ' PRIMARY KEY,
        waga_myszy NUMBER(3) CONSTRAINT waga_myszy_' || KOT.PSEUDO ||' CHECK (waga_myszy BETWEEN 15 AND 45),
        data_zlowienia DATE CONSTRAINT data_zlowienia_nn_' || KOT.PSEUDO ||' NOT NULL)';
    END LOOP;
EXCEPTION
 WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/

-- Procedura: Przyjmij myszy ---------------------------------------------------

CREATE OR REPLACE PROCEDURE przyjmij_myszy_od_kota(ps Kocury.pseudo%TYPE, data_z DATE) AS
    ile_pseudo NUMBER;
    PSEUDO_NIEPRAWIDLOWE_EXCEPTION EXCEPTION;
    
    TYPE MyszyTable IS TABLE OF MYSZY%ROWTYPE INDEX BY BINARY_INTEGER;
    lista_myszy MyszyTable;
    
    TYPE MyszyKotaType IS RECORD (nr_myszy MYSZY.nr_myszy%TYPE, waga_myszy MYSZY.waga_myszy%TYPE, data_zlowienia MYSZY.data_zlowienia%TYPE);
    TYPE MyszyKotaTable IS TABLE OF MyszyKotaType INDEX BY BINARY_INTEGER;
    upolowane_myszy MyszyKotaTable;
    
    indeks NUMBER;
BEGIN 
    SELECT  COUNT(*)
    INTO    ile_pseudo
    FROM    Kocury
    WHERE   pseudo=ps;
    
    IF ile_pseudo <> 1 THEN
        RAISE PSEUDO_NIEPRAWIDLOWE_EXCEPTION;
    END IF;
    
    SELECT  MAX(nr_myszy) + 1
    INTO    indeks 
    FROM    Myszy;
    
    EXECUTE IMMEDIATE 'SELECT * FROM ZLOWIONE_MYSZY_' || ps || ' WHERE data_zlowienia=''' || data_z || ''''
    BULK COLLECT INTO upolowane_myszy;
    
    FOR i IN 1 .. upolowane_myszy.COUNT LOOP
        lista_myszy(i).nr_myszy := indeks;
        lista_myszy(i).waga_myszy := upolowane_myszy(i).waga_myszy;
        lista_myszy(i).data_zlowienia := upolowane_myszy(i).data_zlowienia;
        indeks := indeks + 1;
    END LOOP;
    
    FORALL i IN 1..lista_myszy.COUNT
        INSERT INTO Myszy VALUES(
            lista_myszy(i).nr_myszy,
            ps,
            NULL,
            lista_myszy(i).waga_myszy,
            lista_myszy(i).data_zlowienia,
            NULL
        );
    
    EXECUTE IMMEDIATE 'DELETE FROM ZLOWIONE_MYSZY_' || ps || ' WHERE data_zlowienia=''' || TO_DATE(data_z) || '''';
EXCEPTION
    WHEN PSEUDO_NIEPRAWIDLOWE_EXCEPTION THEN DBMS_OUTPUT.PUT_LINE('Podany pseudonim jest nieprawidlowy!');
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
END przyjmij_myszy_od_kota;
/

SELECT * FROM Myszy ORDER BY 5 DESC;

DELETE FROM ZLOWIONE_MYSZY_TYGRYS;
INSERT INTO ZLOWIONE_MYSZY_TYGRYS VALUES(1,23,TO_DATE('2018-01-09'));
INSERT INTO ZLOWIONE_MYSZY_TYGRYS VALUES(2,32,TO_DATE('2018-01-09'));
INSERT INTO ZLOWIONE_MYSZY_TYGRYS VALUES(3,39,TO_DATE('2018-01-09'));

EXECUTE przyjmij_myszy_od_kota('TYGRYS',TO_DATE('2018-01-09'));

SELECT * FROM ZLOWIONE_MYSZY_TYGRYS;
SELECT * FROM Myszy ORDER BY 5 DESC;

-- Procedura: Wyplata ----------------------------------------------------------

CREATE OR REPLACE PROCEDURE wyplata AS
    koty_indeks NUMBER:=1;
    myszy_indeks NUMBER:=1;
    suma_przydzialow NUMBER:=0;
    przydzielono_mysz BOOLEAN;

    najblizsza_sroda DATE;

    TYPE MyszyTable IS TABLE OF Myszy%ROWTYPE INDEX BY BINARY_INTEGER;
    lista_myszy MyszyTable;

    TYPE PseudoMyszyPair IS RECORD (pseudo Kocury.pseudo%TYPE, myszy NUMBER(3));
    TYPE PseudoMyszyPairTable IS TABLE OF PseudoMyszyPair INDEX BY BINARY_INTEGER;
    lista_kotow PseudoMyszyPairTable;
BEGIN
    SELECT  * BULK COLLECT INTO lista_myszy
    FROM    Myszy
    WHERE   zjadacz IS NULL;
    
    SELECT  NEXT_DAY(LAST_DAY(SYSDATE) - 7, 3) INTO najblizsza_sroda FROM Dual;
    
    SELECT      pseudo, 
                k.myszy_lacznie() BULK COLLECT INTO lista_kotow
    FROM        KocuryNakl k
    WHERE       w_stadku_od <= NEXT_DAY(LAST_DAY(ADD_MONTHS(SYSDATE, -1)) - 7, 3)
    START WITH  szef IS NULL
    CONNECT BY  PRIOR pseudo = szef
    ORDER BY    LEVEL ASC;
    
    FOR i IN 1 .. lista_kotow.COUNT LOOP
        suma_przydzialow := suma_przydzialow + lista_kotow(i).myszy;
    END LOOP;
    
    WHILE myszy_indeks <= lista_myszy.COUNT AND suma_przydzialow> 0 LOOP
        przydzielono_mysz:=FALSE;
        WHILE NOT przydzielono_mysz LOOP
            IF lista_kotow(koty_indeks).myszy > 0 THEN
                lista_myszy(myszy_indeks).zjadacz       := lista_kotow(koty_indeks).pseudo;
                lista_myszy(myszy_indeks).data_wydania  := najblizsza_sroda;
                lista_kotow(koty_indeks).myszy          := lista_kotow(koty_indeks).myszy-1;
                suma_przydzialow := suma_przydzialow - 1;
                przydzielono_mysz := true;
                myszy_indeks := myszy_indeks + 1;
            END IF;
            koty_indeks := MOD(koty_indeks, lista_kotow.COUNT) + 1;
        END LOOP;
    END LOOP;
    
    FORALL i IN 1..lista_myszy.COUNT SAVE EXCEPTIONS
        UPDATE  Myszy
        SET     data_wydania = lista_myszy(i).data_wydania,
                zjadacz = lista_myszy(i).zjadacz
        WHERE   nr_myszy = lista_myszy(i).nr_myszy;
    
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
END wyplata;
/
EXECUTE wyplata;
  
select * from myszy order by 5 desc;

commit;


EXECUTE wyplata;
  
select * from myszy order by 5 desc;



