ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD';
COMMIT;

-- create user c##kuba identified by haslix;
-- create user c##aga identified by zlota;
-- create user c##kuba identified by haslix;
-- create user c##ola identified by ola;
-- create user c##aga identified by zlota;
-- grant all privileges to c##ola identified by ola;

DROP TABLE Wrogowie_Kocurow CASCADE CONSTRAINTS;
DROP TABLE Kocury CASCADE CONSTRAINTS;
DROP TABLE Wrogowie CASCADE CONSTRAINTS;
DROP TABLE Funkcje CASCADE CONSTRAINTS;
DROP TABLE Bandy CASCADE CONSTRAINTS;

COMMIT;

CREATE TABLE Bandy(
    nr_bandy            NUMBER(2)       CONSTRAINT bandy_nr_bandy_pk PRIMARY KEY,
    nazwa               VARCHAR2(20)    CONSTRAINT bandy_nazwa_nn NOT NULL,
    teren               VARCHAR2(15)    CONSTRAINT bandy_teren_uq UNIQUE,
    szef_bandy          VARCHAR2(15)    CONSTRAINT bandy_szef_bandy_uq UNIQUE
);

CREATE TABLE Funkcje(
    funkcja             VARCHAR2(10)    CONSTRAINT funkcje_funkcja_pk PRIMARY KEY,
    min_myszy           NUMBER(3)       CONSTRAINT funkcje_min_myszy_min CHECK (5 < min_myszy),
    max_myszy           NUMBER(3)       CONSTRAINT funkcje_max_myszy_max CHECK (max_myszy < 200),
    CONSTRAINT funkcje_max_myszy_max_valid CHECK (min_myszy <= max_myszy)
);

CREATE TABLE Wrogowie(
    imie_wroga          VARCHAR2(15)    CONSTRAINT wrogowie_imie_wroga_pk PRIMARY KEY,
    stopien_wrogosci    NUMBER(2)       CONSTRAINT wrogowie_stopien_wrogosci_bt CHECK (stopien_wrogosci BETWEEN 1 AND 10),
    gatunek             VARCHAR2(15),
    lapowka             VARCHAR2(20)
);

CREATE TABLE Kocury(
    imie                VARCHAR2(15)    CONSTRAINT kocury_imie_nn NOT NULL,
    plec                VARCHAR2(1)     CONSTRAINT kocury_plec_in CHECK (plec IN ('M', 'D')),
    pseudo              VARCHAR2(15)    CONSTRAINT kocury_pseudo_pk PRIMARY KEY,
    funkcja             VARCHAR2(10)    CONSTRAINT kocury_funkcja_fk REFERENCES Funkcje(funkcja),
    szef                VARCHAR2(15)    CONSTRAINT kocury_szef_fk REFERENCES Kocury(pseudo),
    w_stadku_od         DATE            DEFAULT SYSDATE,
    przydzial_myszy     NUMBER(3),
    myszy_extra         NUMBER(3),
    nr_bandy            NUMBER(2)       CONSTRAINT kocury_nr_bandy_fk REFERENCES Bandy(nr_bandy)
);

ALTER TABLE Bandy ADD CONSTRAINT bandy_szef_bandy_fk FOREIGN KEY (szef_bandy) REFERENCES Kocury(pseudo);

CREATE TABLE Wrogowie_Kocurow(
    pseudo              VARCHAR2(15)    CONSTRAINT wrog_koc_pseudo_fk REFERENCES Kocury(pseudo),
    imie_wroga          VARCHAR2(15)    CONSTRAINT wrog_koc_imie_wroga_fk REFERENCES Wrogowie(imie_wroga),
    data_incydentu      DATE            CONSTRAINT wrog_koc_data_incydentu_nn NOT NULL,
    opis_incydentu      VARCHAR2(50),
    CONSTRAINT wrogowie_kocurow_pk PRIMARY KEY(pseudo, imie_wroga)
);

COMMIT;
