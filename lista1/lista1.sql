--Zadanie 1
SELECT  imie_wroga      WROG,
        opis_incydentu  PRZEWINA
FROM    Wrogowie_Kocurow
WHERE   EXTRACT(YEAR from data_incydentu) = 2009;

--Zadanie 2
SELECT  imie,
        funkcja,
        w_stadku_od "Z NAMI OD"
FROM    Kocury
WHERE   plec='D'
AND     w_stadku_od BETWEEN '2005-09-01' AND '2007-07-31';

--Zadanie 3
SELECT      imie_wroga          WROG,
            gatunek,
            stopien_wrogosci    "STOPIEN WROGOSCI"
FROM        Wrogowie
WHERE       lapowka IS NULL
ORDER BY    stopien_wrogosci;

--Zadanie 4 DUDEK zwany MALY (fun. KOT) lowi myszki w bandzie 4 od 2011-05-15
SELECT      imie || ' zwany ' || pseudo || ' (fun. ' || funkcja || ') lowi myszki w bandzie ' || nr_bandy || ' od ' || w_stadku_od "WSZYSTKO O KOCURACH"
FROM        Kocury
ORDER BY    w_stadku_od DESC, pseudo;

--Zadanie 5
SELECT  pseudo, 
        regexp_replace(regexp_replace(pseudo, 'A', '#', 1, 1), 'L', '%', 1, 1) "Po wymianie A na # oraz L na %"
FROM    Kocury
WHERE   pseudo LIKE '%A%'
AND     pseudo LIKE '%L%';

--Zadanie 6
SELECT  imie, w_stadku_od "W stadku", przydzial_myszy "Zjadl", ADD_MONTHS(w_stadku_od, 6) "Podywzka", ROUND(przydzial_myszy * 1.1) "Zjada"
FROM    Kocury
WHERE   EXTRACT(MONTH FROM w_stadku_od) BETWEEN 3 AND 9
AND     EXTRACT(YEAR FROM w_stadku_od) <= EXTRACT(YEAR FROM SYSDATE) - 8;

--Zadanie 7
SELECT  imie, przydzial_myszy * 3 "MYSZY KWARTALNIE", NVL(myszy_extra,0)*3 "KWARTALNE DODATKI"
FROM    Kocury
WHERE   przydzial_myszy > 2 * NVL(myszy_extra,0)
AND     przydzial_myszy >= 55;

--Zadanie 8
SELECT  imie,
        CASE WHEN przydzial_myszy * 12 < 660 
            THEN 'Ponizej 660' 
            ELSE CASE WHEN przydzial_myszy * 12 = 660 
                THEN 'Limit' 
                ELSE TO_CHAR(przydzial_myszy * 12)
            END
        END "Zjada rocznie"
FROM    Kocury;

--Zadanie 9 TO_DATE('2017-10-12')
SELECT  pseudo,
        w_stadku_od "W STADKU", 
        CASE WHEN SYSDATE <= LAST_DAY('2017-10-12')
            THEN CASE WHEN EXTRACT(DAY FROM w_stadku_od) <= 15
                THEN NEXT_DAY(LAST_DAY(SYSDATE), 3) - 7
                ELSE NEXT_DAY(LAST_DAY(ADD_MONTHS(SYSDATE, 1)), 3) - 7
            END
            ELSE NEXT_DAY(LAST_DAY(SYSDATE), 3) - 7
        END WYPLATA
FROM    Kocury;

SELECT  pseudo,
        w_stadku_od "W STADKU",
        CASE WHEN SYSDATE <= NEXT_DAY(LAST_DAY(SYSDATE) - 7, 3)
            THEN CASE WHEN EXTRACT(DAY FROM KOCURY.W_STADKU_OD) <= 15
                THEN NEXT_DAY(LAST_DAY('2017-10-23') - 7, 3)
                ELSE NEXT_DAY(LAST_DAY(ADD_MONTHS('2017-10-23', 1)) - 7, 3)
            END
            ELSE NEXT_DAY(LAST_DAY(ADD_MONTHS('2017-10-23', 1)) - 7, 3)
        END "WYPLATA"
FROM KOCURY;

----

SELECT  pseudo,
        w_stadku_od     "W stadku",
        CASE WHEN EXTRACT(DAY FROM w_stadku_od) <= 15 AND NEXT_DAY(LAST_DAY(SYSDATE) - 7, 3) >= SYSDATE
            THEN NEXT_DAY(LAST_DAY(SYSDATE) - 7, 3)
            ELSE NEXT_DAY(LAST_DAY(ADD_MONTHS(SYSDATE, 1)) - 7, 3)
        END             Wyplata
FROM    Kocury;

SELECT  pseudo,
        w_stadku_od     "W stadku",
        NEXT_DAY(LAST_DAY(ADD_MONTHS(SYSDATE, 1)) - 7, 3) Wyplata
FROM    Kocury;

--Zadanie 10a
SELECT  pseudo
||      ' - '
||      DECODE(COUNT(pseudo), 1,' - Unikalny',' - Nieunikalny') "Unikalnosc atr. PSEUDO"
FROM    Kocury k2;

--Zadanie 10b
SELECT      szef 
||          ' - ' 
||          DECODE(COUNT(szef), 1,' - Unikalny',' - Nieunikalny') "Unikalnosc atr. SZEF"
FROM        Kocury k2
WHERE       szef IS NOT NULL
GROUP BY    szef;

--Zadanie 11
SELECT      pseudo "Pseudonim",
            COUNT(pseudo) "Liczba wrogow"
FROM        Wrogowie_Kocurow
GROUP BY    pseudo
HAVING      COUNT(pseudo) >= 2;

--Zadanie 12
SELECT      'Liczba kotow = ' 
||          COUNT(pseudo) 
||          ' lowi jako ' 
||          funkcja 
||          ' i zjada max.' 
||          TO_CHAR(MAX(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)), '99.99') 
||          ' myszy miesiecznie' " "
FROM        Kocury
WHERE       pseudo != 'SZEFUNIO' 
AND         plec != 'M'
GROUP BY    funkcja
HAVING      AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra,0)) > 50;

SELECT      'Liczba kotow=' " ", 
            COUNT(pseudo) " ",
            'lowi jako'  " ",
            funkcja " ",
            ' i zjada max.' " ",
            TO_CHAR(MAX(NVL(przydzial_myszy, 0) + NVL(myszy_extra, 0)), '99.99') " ",
           ' myszy miesiecznie' " "
FROM        Kocury
WHERE       pseudo != 'SZEFUNIO' 
AND         plec != 'M'
GROUP BY    funkcja
HAVING      AVG(NVL(przydzial_myszy, 0) + NVL(myszy_extra,0)) > 50;

--Zadanie 13
SELECT      nr_bandy                        "Nr bandy",
            plec,
            MIN(NVL(przydzial_myszy, 0))    "Minimalny przydzial"
FROM        Kocury
GROUP BY    nr_bandy, plec;

--Zadanie 14
SELECT      LEVEL       Poziom,
            pseudo      Pseudonim,
            funkcja,
            nr_bandy    "Nr bandy"
FROM        Kocury
WHERE       plec = 'M'
CONNECT BY  PRIOR pseudo = szef
START WITH  funkcja = 'BANDZIOR'
ORDER BY    nr_bandy, LEVEL;

--Zadanie 15 
SELECT      LPAD(LEVEL - 1, (LEVEL - 1) * 4 + LENGTH(LEVEL), '===>') ||
            '       ' ||
            imie Hierarchia,
            NVL(szef, 'Sam sobie panem') "PSEUDO SZEFA",
            funkcja
FROM        Kocury
WHERE       myszy_extra IS NOT NULL
CONNECT BY  PRIOR pseudo = szef
START WITH  szef IS NULL;

--Zadanie 16
SELECT      LPAD(pseudo, (level - 1) * 4 + length(pseudo), ' ') "Droga sluzbowa"
FROM        Kocury
CONNECT BY  PRIOR szef = pseudo
START WITH  myszy_extra IS NULL
AND         plec = 'M'
AND         ADD_MONTHS(w_stadku_od, 6 * 12) < '2015-08-01';

