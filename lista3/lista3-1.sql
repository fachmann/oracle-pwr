DROP TABLE monitor;

CREATE TABLE monitor (
    kto     VARCHAR2(20),
    kiedy   DATE,
    komu    VARCHAR2(15),
    co      VARCHAR2(6)
);

CREATE OR REPLACE TRIGGER ogrfunkcj2 BEFORE
    INSERT OR UPDATE ON kocury
    FOR EACH ROW
DECLARE
    mini        funkcje.min_myszy%TYPE;
    maxi        funkcje.max_myszy%TYPE;
    czytabela   NUMBER;
    co          VARCHAR2(6);
    PRAGMA AUTONOMOUS_TRANSACTION; 
BEGIN
    SELECT        min_myszy,        max_myszy
    INTO        mini,maxi
    FROM        funkcje
    WHERE
        :new.funkcja = funkcja;

    dbms_output.put(mini);
    IF
        :new.przydzial_myszy < mini OR :new.przydzial_myszy > maxi
    THEN
        dbms_output.put('TTTTT');
        IF
            inserting
        THEN
            co := 'INSERT';
        ELSE
            co := 'UPDATE';
        END IF;
        EXECUTE IMMEDIATE ( 'INSERT INTO Monitor VALUES (:kto, SYSDATE, :komu, :co)' )
            USING LOGIN_USER,:new.pseudo,co;
        COMMIT;
        raise_application_error(-20020,'Przekroczony zakres myszy');
    END IF;

END;
/

ALTER SESSION SET nls_date_format = 'YYYY-MM-DD';

UPDATE kocury
    SET
        przydzial_myszy = 169
WHERE
    pseudo = 'TYGRYS';

INSERT INTO kocury VALUES (
    'WIKTORIA',
    'D',
    'WIKSA',
    'MILUSIA',
    'LYSY',
    '2012-02-01',
    25,
    260,
    2
);

INSERT INTO kocury VALUES (
    'KAZIK',
    'M',
    'MAKLER',
    'LOWCZY',
    'TYGRYS',
    '2008-12-01',
    165,
    NULL,
    2
);

SELECT
    *
FROM
    kocury;

SELECT
    *
FROM
    monitor;

ROLLBACK;





CREATE SEQUENCE index_dodatki
START WITH 1
INCREMENT BY 1;

CREATE OR REPLACE TRIGGER WirusPrzed 
BEFORE UPDATE OF przydzial_myszy ON Kocury
FOR EACH ROW
BEGIN
    insert into x values ('insert', index_dodatki.nextval);
END;
/

CREATE OR REPLACE TRIGGER WirusPO
AFTER UPDATE OF przydzial_myszy ON Kocury
FOR EACH ROW
BEGIN
    insert into x values ('update', index_dodatki.nextval);
END;
/

select * from kocury where pseudo in ('PLACEK','RURA','LOLA');

create table x ( name varchar(30), no int);

select * from x;
insert into dual values('x');

update kocury set przydzial_myszy = 0 where pseudo in ('PLACEK','RURA','LOLA');

select 'drop trigger ' || trigger_name || ';' stmt from user_triggers;

drop trigger ZAD42_BEFOREUPDATE;
drop trigger ZAD42_AFTERUPDATE;
drop trigger ZAD42_BEFOREUPDATEEACHROW;
drop trigger OGRFUNKCJ2;
drop trigger WIRUSPRZED;