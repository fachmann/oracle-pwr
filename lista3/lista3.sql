SET SERVEROUTPUT ON;

--Zadanie 34
DECLARE
    fun Funkcje.funkcja%TYPE;
BEGIN
    SELECT      funkcja 
    INTO        fun
    FROM        Kocury
    WHERE       funkcja = UPPER('bandzior')
    FETCH NEXT 1 ROWS ONLY;
    DBMS_OUTPUT.PUT_LINE(fun);
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota o podanej funkcji !');
END;
/

--Zadanie 35
DECLARE
    roczny_przydzial Kocury.przydzial_myszy%TYPE;
    kimie Kocury.imie%TYPE;
    data_przystapienia Kocury.w_stadku_od%TYPE;
    kryteria BOOLEAN := FALSE;
BEGIN
    SELECT  NVL(przydzial_myszy, 0) * 12,
            imie,
            w_stadku_od
    INTO    roczny_przydzial,
            kimie,
            data_przystapienia
    FROM    Kocury
    WHERE   pseudo = UPPER('lola');
    
    IF roczny_przydzial > 700 THEN 
        dbms_output.put_line('calkowity roczny przydzial myszy > 700');
        kryteria := TRUE;
    END IF;
    
    IF kimie LIKE '%A%' THEN
        dbms_output.put_line('imie zawiera litere A');
        kryteria := TRUE;
    END IF;
    
    IF EXTRACT(MONTH FROM data_przystapienia) = 1 THEN
        dbms_output.put_line('styczen jest miesiacem przystapienia do stada');
        kryteria := TRUE;
    END IF;
    
    IF NOT kryteria THEN
        dbms_output.put_line('nie odpowiada kryteriom');
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN dbms_output.put_line('Nie znaleziono kota o podanej funkcji !');
END;
/

--Zadanie 36
DECLARE
CURSOR cursor_przydzial IS
  SELECT NVL(przydzial_myszy,0) przydzial_myszy, max_myszy
  FROM Kocury NATURAL JOIN Funkcje
  ORDER BY przydzial_myszy ASC
  FOR UPDATE OF przydzial_myszy;
  
cur_re cursor_przydzial%ROWTYPE;
przydzial_myszy_suma NUMBER;
nowy_przydzial_myszy NUMBER;
liczba_pod NUMBER := 0;
sa_wiersze BOOLEAN := FALSE;
brak_kotow EXCEPTION;

BEGIN
    LOOP EXIT WHEN przydzial_myszy_suma > 1050;
        OPEN cursor_przydzial;
        LOOP
            FETCH cursor_przydzial INTO cur_re;
            EXIT WHEN cursor_przydzial%NOTFOUND;
            
            IF NOT sa_wiersze THEN 
                sa_wiersze := TRUE;
            END IF;
            
            nowy_przydzial_myszy := cur_re.przydzial_myszy * 1.1;
            
            IF nowy_przydzial_myszy > cur_re.max_myszy THEN 
                nowy_przydzial_myszy := cur_re.max_myszy;
            END IF;
            
            IF cur_re.przydzial_myszy < nowy_przydzial_myszy THEN 
                liczba_pod := liczba_pod + 1;
            END IF;
            
            UPDATE Kocury 
            SET przydzial_myszy = nowy_przydzial_myszy
            WHERE CURRENT OF cursor_przydzial;
            
            SELECT SUM(NVL(przydzial_myszy, 0)) INTO przydzial_myszy_suma FROM Kocury;
            IF przydzial_myszy_suma > 1050
            THEN EXIT;
            END IF;
        END LOOP;
        CLOSE cursor_przydzial;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('Calk. przydzial w stadku - ' || przydzial_myszy_suma || ' Zmian - ' || liczba_pod);
    IF NOT sa_wiersze THEN
        RAISE brak_kotow;
    END IF;
EXCEPTION
    WHEN brak_kotow THEN DBMS_OUTPUT.PUT_LINE('Brak kotow');
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/

ROLLBACK;

SELECT imie, nvl(przydzial_myszy,0) "Myszki po podwyzce" 
FROM Kocury 
ORDER BY 2 DESC;

--Zadanie 37
DECLARE
    i NUMBER := 0;
BEGIN
    DBMS_OUTPUT.PUT_LINE('Nr   Pseudonim   Zjada ');
    DBMS_OUTPUT.PUT_LINE('-----------------------');
    FOR elem IN (SELECT pseudo, NVL(przydzial_myszy,0) + NVL(myszy_extra,0) zjada FROM Kocury ORDER BY 2 DESC)
    LOOP
        i := i + 1;
        DBMS_OUTPUT.PUT_LINE(i || '    ' || RPAD(elem.pseudo, 9, ' ') || '   ' || elem.zjada);
        EXIT WHEN i = 5;
    END LOOP;
END;
/

--Zadanie 38
DECLARE
    CURSOR cur_hier IS
           SELECT   imie, 
                    szef
           FROM     Kocury
           WHERE    funkcja IN ('KOT','MILUSIA');
    cur_re cur_hier%ROWTYPE;
    akt_szef Kocury.szef%TYPE;
    akt_imie Kocury.imie%TYPE;
    ilu NUMBER := '&glebokosc';
    maxGlebokosc NUMBER;
BEGIN
    SELECT      MAX(LEVEL) 
    INTO        maxGlebokosc
    FROM        Kocury
    CONNECT BY  PRIOR pseudo = szef
    START WITH  szef IS NULL;
    
    IF ilu > maxGlebokosc THEN
        ilu := maxGlebokosc - 1;
    END IF;
    
    DBMS_OUTPUT.PUT(RPAD('Imie', 12));
    FOR i IN 1 .. ilu LOOP
        DBMS_OUTPUT.PUT(RPAD('|  Szef' || i, 15));
    END LOOP;
    
    DBMS_OUTPUT.NEW_LINE();
    
    DBMS_OUTPUT.PUT(LPAD(' ', 11, '-'));
    FOR i IN 1 .. ilu LOOP
        DBMS_OUTPUT.PUT(LPAD(' ', 4, '-'));
        DBMS_OUTPUT.PUT(LPAD(' ', 11, '-'));
    END LOOP;
    
    DBMS_OUTPUT.NEW_LINE();
    
    OPEN cur_hier;
    LOOP
        FETCH cur_hier INTO cur_re; 
        EXIT WHEN cur_hier%NOTFOUND;
        DBMS_OUTPUT.PUT(RPAD(cur_re.imie, 12));
        akt_szef := cur_re.szef;
        FOR i IN 1..ilu LOOP
            IF akt_szef IS NOT NULL THEN
                SELECT imie, szef INTO akt_imie, akt_szef FROM Kocury WHERE pseudo = akt_szef;
                DBMS_OUTPUT.PUT('|  ' || RPAD(akt_imie,12));
            ELSE
                DBMS_OUTPUT.PUT('|  ' || RPAD(' ',12));
            END IF;
        END LOOP;
        DBMS_OUTPUT.NEW_LINE();
    END LOOP;
    CLOSE cur_hier;
    DBMS_OUTPUT.NEW_LINE();
END;
/

--Zadanie 39
DECLARE
    nrB             Bandy.nr_bandy%TYPE := '&numer_bandy';
    nazwaB          Bandy.nazwa%TYPE    := '&nazwa_bandy';
    terenB          Bandy.teren%TYPE    := '&teren_bandy';
    czyJestDuplikat BOOLEAN             := FALSE;
    dup_values      EXCEPTION;
    less_zero       EXCEPTION;
    null_values     EXCEPTION;
    numNr           NUMBER;
    numNaz          NUMBER;
    numTer          NUMBER;
BEGIN
    SELECT COUNT(*) INTO numNr  FROM Bandy WHERE nrB    = nr_bandy;
    SELECT COUNT(*) INTO numNaz FROM Bandy WHERE nazwaB = nazwa;
    SELECT COUNT(*) INTO numTer FROM Bandy WHERE terenB = teren;
    
    IF numNr > 0 THEN
        DBMS_OUTPUT.PUT(nrB || ', ');
        czyJestDuplikat := TRUE;
    END IF;
    
    IF numNaz > 0 THEN
        DBMS_OUTPUT.PUT(nazwaB || ', ');
    END IF;
    
    IF numTer > 0 THEN
        DBMS_OUTPUT.PUT(terenB);
        czyJestDuplikat := TRUE;
    END IF;
    
    IF czyJestDuplikat THEN
        RAISE dup_values;
    END IF;
    
    IF  nrB     IS NULL 
    OR  nazwaB  IS NULL 
    OR  terenB  IS NULL 
    THEN
        RAISE null_values;
    END IF;
    
    IF nrB <= 0 THEN
        RAISE less_zero;
    END IF;
    
    INSERT INTO Bandy(nr_bandy, nazwa, teren) VALUES (nrB, nazwaB, terenB);
    DBMS_OUTPUT.PUT_LINE('Pola dodane pomyslnie !');
EXCEPTION
    WHEN dup_values   THEN DBMS_OUTPUT.PUT_LINE(' :juz istnieje');
    WHEN less_zero    THEN DBMS_OUTPUT.PUT_LINE('Numer bandy <= 0');
    WHEN null_values  THEN DBMS_OUTPUT.PUT_LINE('Niewprowadzono wszystkich danych !');
    WHEN OTHERS       THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/

SELECT * FROM Bandy;
ROLLBACK;

--Zadanie 40
CREATE OR REPLACE PROCEDURE nowaBanda(
        nrB       Bandy.nr_bandy%TYPE,
        nazwaB    Bandy.nazwa%TYPE,
        terenB    Bandy.teren%TYPE
    ) AS
    duplikat      BOOLEAN := FALSE;
    dup_values    EXCEPTION;
    less_zero     EXCEPTION;
    null_values   EXCEPTION;
    numNr         NUMBER;
    numNaz        NUMBER;
    numTer        NUMBER;
BEGIN
    IF  nrB IS NOT NULL
    AND nrB <= 0 THEN
        RAISE less_zero;
    END IF;
    
    SELECT COUNT(*) INTO numNr  FROM Bandy WHERE nrB    = nr_bandy;
    SELECT COUNT(*) INTO numNaz FROM Bandy WHERE nazwaB = nazwa;
    SELECT COUNT(*) INTO numTer FROM Bandy WHERE terenB = teren;
    
    IF numNr > 0 THEN
        DBMS_OUTPUT.PUT(nrB || ', ');
        duplikat := TRUE;
    END IF;
    
    IF numNaz > 0 THEN
        DBMS_OUTPUT.PUT(nazwaB || ', ');
        duplikat := TRUE;
    END IF;
    
    IF numTer > 0 THEN
        DBMS_OUTPUT.PUT(terenB);
        duplikat := TRUE;
    END IF;
    
    IF duplikat THEN
        RAISE dup_values;
    END IF;
    
    IF  nazwaB  IS NULL 
    OR  terenB  IS NULL THEN
        RAISE null_values;
    END IF;
    
    INSERT INTO Bandy (nr_bandy, nazwa, teren) VALUES (nrB, nazwaB, terenB);
    DBMS_OUTPUT.PUT_LINE('Pola dodane pomyslnie !');
EXCEPTION
    WHEN DUP_VALUES   THEN DBMS_OUTPUT.PUT_LINE(' :juz istnieje');
    WHEN LESS_ZERO    THEN DBMS_OUTPUT.PUT_LINE('Numer bandy <= 0');
    WHEN OTHERS       THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/

BEGIN
    NOWABANDA(null, 'a', 'a');
    NOWABANDA(10, 'b', 'b');
END;
/

CALL NOWABANDA(10, 'c', 'c');
SELECT * FROM Bandy;
ROLLBACK;

--Zadanie 41
CREATE OR REPLACE TRIGGER nr_nowej_bandy
BEFORE INSERT ON Bandy
FOR EACH ROW
DECLARE
    numer Bandy.nr_bandy%TYPE := 1;
BEGIN
    SELECT MAX(nr_bandy) INTO numer FROM Bandy;
    :NEW.nr_bandy := numer + 1;
END;
/

INSERT INTO Bandy (nr_bandy, nazwa, teren) VALUES (null, 'nazwaB2', 'terenB2');
SELECT MAX(nr_bandy) FROM Bandy;
DELETE FROM Bandy WHERE nr_bandy > 5;
COMMIT;

SHOW ERRORS;

--Zadanie 42a

--propozycja 1

CREATE OR REPLACE PACKAGE Wirus AS
    akt_pseudo      Kocury.pseudo%TYPE;
    czyWykonac      BOOLEAN := TRUE;
    nowyPrzydzial   Kocury.przydzial_myszy%TYPE;
    staryPrzydzial  Kocury.przydzial_myszy%TYPE;
    
    FUNCTION przydzial_tygrysa RETURN Kocury.przydzial_myszy%TYPE;
END Wirus;
/

CREATE OR REPLACE PACKAGE BODY Wirus AS
    FUNCTION przydzial_tygrysa RETURN Kocury.przydzial_myszy%TYPE IS
    przydzial Kocury.przydzial_myszy%TYPE;
    BEGIN
        SELECT  przydzial_myszy 
        INTO    przydzial
        FROM    Kocury
        WHERE   pseudo = 'TYGRYS';
        RETURN przydzial;
    END przydzial_tygrysa;
END Wirus;
/

CREATE OR REPLACE TRIGGER WirusPrzed 
BEFORE UPDATE OF przydzial_myszy ON Kocury
FOR EACH ROW WHEN(NEW.funkcja = 'MILUSIA')
DECLARE
BEGIN
    IF (Wirus.czyWykonac) THEN
        Wirus.akt_pseudo := :NEW.pseudo;
        IF :NEW.przydzial_myszy < :OLD.przydzial_myszy THEN
            :NEW.przydzial_myszy := :OLD.przydzial_myszy;
        END IF;
        Wirus.nowyPrzydzial := :NEW.przydzial_myszy;
        Wirus.staryPrzydzial := :OLD.przydzial_myszy;
    END IF;
END;
/

CREATE OR REPLACE TRIGGER WirusPo
AFTER UPDATE OF przydzial_myszy ON Kocury
BEGIN
    IF (Wirus.czyWykonac) THEN
        Wirus.czyWykonac := FALSE;
        IF ((Wirus.nowyPrzydzial - Wirus.staryPrzydzial) < Wirus.przydzial_tygrysa * 0.1) THEN
            UPDATE  Kocury
            SET     przydzial_myszy = Wirus.nowyPrzydzial, 
                    myszy_extra = myszy_extra + 5
            WHERE   pseudo = Wirus.akt_pseudo;
            
            UPDATE  Kocury
            SET     przydzial_myszy = przydzial_myszy * 0.9
            WHERE   pseudo = 'TYGRYS';
        ELSE
            UPDATE  Kocury
            SET     myszy_extra = myszy_extra + 5
            WHERE   pseudo = 'TYGRYS';
            
            UPDATE  Kocury
            SET     przydzial_myszy = Wirus.nowyPrzydzial
            WHERE   pseudo = Wirus.akt_pseudo;
        END IF;
        Wirus.czyWykonac := TRUE;
    END IF;
END;
/

-- propozycja 2

CREATE OR REPLACE PACKAGE Wirus AS
    dobrze_dodane       NUMBER  := 0;
    przydzial_tyg   NUMBER DEFAULT 0;
END Wirus;
/

CREATE OR REPLACE TRIGGER WirusPrzedTygrys
BEFORE UPDATE ON Kocury
BEGIN
    SELECT  przydzial_myszy
    INTO    Wirus.przydzial_tyg
    FROM    Kocury
    WHERE   pseudo = 'TYGRYS';
END;
/

CREATE OR REPLACE TRIGGER WirusPrzed 
BEFORE UPDATE OF przydzial_myszy ON Kocury
FOR EACH ROW WHEN(NEW.funkcja = 'MILUSIA')
BEGIN
    IF ((:NEW.przydzial_myszy - :OLD.przydzial_myszy) < Wirus.przydzial_tyg * 0.1) THEN
        Wirus.dobrze_dodane := Wirus.dobrze_dodane - 1;
        :NEW.myszy_extra := :NEW.myszy_extra + 5;
    ELSE
        Wirus.dobrze_dodane := Wirus.dobrze_dodane + 1;
    END IF;
    
    IF :NEW.przydzial_myszy < :OLD.przydzial_myszy THEN
        :NEW.przydzial_myszy := :OLD.przydzial_myszy;
    END IF;
END;
/

CREATE OR REPLACE TRIGGER WirusPo
AFTER UPDATE OF przydzial_myszy ON Kocury
DECLARE
    tmp   NUMBER DEFAULT 0;
BEGIN
    tmp := Wirus.dobrze_dodane;
    Wirus.dobrze_dodane := 0;
    IF (tmp > 0) THEN
        UPDATE  Kocury
        SET     myszy_extra = myszy_extra + 5 * tmp
        WHERE   pseudo = 'TYGRYS';
    END IF;
    IF (tmp < 0) THEN
        UPDATE  Kocury
        SET     przydzial_myszy = przydzial_myszy * POWER(0.9, -tmp)
        WHERE   pseudo = 'TYGRYS';
    END IF;
END;
/

SELECT pseudo, funkcja, przydzial_myszy, myszy_extra 
FROM Kocury
WHERE funkcja = 'MILUSIA' OR pseudo = 'TYGRYS';

UPDATE Kocury 
SET przydzial_myszy = przydzial_myszy + 15
WHERE pseudo IN ('MALA','LASKA');

ROLLBACK;

DROP TRIGGER WirusPo;
DROP TRIGGER WirusPrzed;
DROP TRIGGER WirusPrzedTygrys;
DROP PACKAGE Wirus;

--Zadanie 42b
CREATE OR REPLACE TRIGGER Compound_Wirus
FOR UPDATE OF przydzial_myszy ON Kocury
WHEN (OLD.funkcja = 'MILUSIA')
COMPOUND TRIGGER
    
    tmp       NUMBER  := 0;
    dobrze_dodane       NUMBER  := 0;
    przydzial_tygrysa   Kocury.przydzial_myszy%TYPE;
    
    BEFORE STATEMENT IS 
    BEGIN
        SELECT przydzial_myszy 
        INTO przydzial_tygrysa
        FROM Kocury
        WHERE pseudo = 'TYGRYS';
    END BEFORE STATEMENT;
  
    BEFORE EACH ROW IS
    BEGIN
        IF ((:NEW.przydzial_myszy - :OLD.przydzial_myszy) < przydzial_tygrysa * 0.1) THEN
            dobrze_dodane := dobrze_dodane - 1;
            :NEW.myszy_extra := :NEW.myszy_extra + 5;
        ELSE
            dobrze_dodane := dobrze_dodane + 1;
        END IF;
        
        IF :NEW.przydzial_myszy < :OLD.przydzial_myszy THEN
            :NEW.przydzial_myszy := :OLD.przydzial_myszy;
        END IF;
    END BEFORE EACH ROW;
  
    AFTER STATEMENT IS
    BEGIN
        tmp := dobrze_dodane;
        dobrze_dodane := 0;
        IF (tmp > 0) THEN
            UPDATE  Kocury
            SET     myszy_extra = myszy_extra + 5 * tmp
            WHERE   pseudo = 'TYGRYS';
        END IF;
        IF (tmp < 0) THEN
            UPDATE  Kocury
            SET     przydzial_myszy = przydzial_myszy * POWER(0.9, -tmp)
            WHERE   pseudo = 'TYGRYS';
        END IF;
    END AFTER STATEMENT;
END compound_wirus;
/

SELECT pseudo, funkcja, przydzial_myszy, myszy_extra 
FROM Kocury
WHERE funkcja = 'MILUSIA' OR pseudo = 'TYGRYS';

UPDATE Kocury 
SET przydzial_myszy = przydzial_myszy + 15
WHERE pseudo IN ('MALA','LASKA');

ROLLBACK;

DROP TRIGGER Compound_Wirus;

--Zadanie 43
DECLARE
TYPE MAP_STRING_INT IS TABLE OF NUMBER INDEX BY Funkcje.funkcja%TYPE;

suma_funkcji    MAP_STRING_INT;
i               NUMBER := 0;
suma_banda      NUMBER;
suma_calosc     NUMBER := 0;
BEGIN
    DBMS_OUTPUT.PUT(RPAD('Nazwa Bandy',16));
    DBMS_OUTPUT.PUT(RPAD('Plec',7));
    DBMS_OUTPUT.PUT(RPAD('Ile',8));
    FOR funkcja IN (SELECT funkcja FROM Funkcje) LOOP
        DBMS_OUTPUT.PUT(RPAD(funkcja.funkcja,13));
        suma_funkcji(funkcja.funkcja) := 0;
    END LOOP;
    DBMS_OUTPUT.PUT(RPAD('SUMA',13));
    DBMS_OUTPUT.NEW_LINE();
    
    DBMS_OUTPUT.PUT('-------------- ------- ------');
    FOR i IN 1 .. (suma_funkcji.COUNT + 1) LOOP
        DBMS_OUTPUT.PUT(RPAD(' ',13,'-'));
    END LOOP;
    DBMS_OUTPUT.NEW_LINE();
    
    FOR banda IN (
            SELECT      nr_bandy,
                        MIN(nazwa) nazwa,
                        plec,
                        COUNT(pseudo) ile 
            FROM        Kocury NATURAL JOIN Bandy
            GROUP BY    nr_bandy, plec 
            ORDER BY    2, 3 ASC) LOOP
        IF MOD(i, 2) = 0 THEN
            DBMS_OUTPUT.PUT(RPAD(banda.nazwa, 16));
            DBMS_OUTPUT.PUT(RPAD('Kotka', 7));
        ELSE
            DBMS_OUTPUT.PUT(LPAD(' ', 16));
            DBMS_OUTPUT.PUT(RPAD('Kocor', 7));
        END IF;
        
        DBMS_OUTPUT.PUT(RPAD(banda.ile,8));
        
        suma_banda := 0; 
        FOR funkcja IN (
                SELECT      f.funkcja, 
                            SUM(NVL(k.przydzial_myszy, 0) + NVL(k.myszy_extra, 0)) suma
                FROM        Funkcje f 
                LEFT JOIN   Kocury k ON f.funkcja = k.funkcja 
                AND         k.nr_bandy = banda.nr_bandy
                AND         k.plec = banda.plec
                GROUP BY    f.funkcja
                ORDER BY    1) LOOP
            suma_banda := suma_banda + funkcja.suma;
            suma_funkcji(funkcja.funkcja) := suma_funkcji(funkcja.funkcja) + funkcja.suma;
            DBMS_OUTPUT.PUT(RPAD(funkcja.suma,13));
        END LOOP;
        
        DBMS_OUTPUT.PUT_LINE(RPAD(suma_banda,13));
        
        suma_calosc := suma_calosc + suma_banda;
        i := i + 1;
    END LOOP;
    
    DBMS_OUTPUT.PUT('z------------- ------- ------');
    FOR i IN 1 .. (suma_funkcji.COUNT + 1) LOOP
        DBMS_OUTPUT.PUT(RPAD(' ',13,'-'));
    END LOOP;
    DBMS_OUTPUT.NEW_LINE();
    
    DBMS_OUTPUT.PUT(RPAD('ZJADA RAZEM',31));
    
    FOR funkcja IN (SELECT funkcja FROM Funkcje) LOOP
        DBMS_OUTPUT.PUT(RPAD(suma_funkcji(funkcja.funkcja),13));
    END LOOP;
    
    DBMS_OUTPUT.PUT_LINE(suma_calosc);
END;
/

--Zadanie 44
CREATE OR REPLACE PACKAGE podatek_poglowny AS
    FUNCTION podatek(ps Kocury.pseudo%TYPE) RETURN NUMBER;
    PROCEDURE nowaBanda(nrB Bandy.nr_bandy%TYPE, nazwaB Bandy.nazwa%TYPE, terenB Bandy.teren%TYPE);
END podatek_poglowny;
/

CREATE OR REPLACE PACKAGE BODY podatek_poglowny AS
    FUNCTION podatek(ps Kocury.pseudo%TYPE) RETURN NUMBER IS
        sumaPod     NUMBER := 0;
        temp        NUMBER := 0;
    BEGIN
        SELECT  CEIL((NVL(przydzial_myszy, 0) + NVL(myszy_extra,0)) * 0.05)
        INTO    sumaPod
        FROM    Kocury
        WHERE   pseudo = ps;
        
        SELECT  COUNT(pseudo)
        INTO    temp 
        FROM    Kocury
        WHERE   szef = ps;
        
        IF temp = 0 THEN
            sumaPod := sumaPod + 2;
        END IF;
        
        SELECT  COUNT(pseudo) 
        INTO    temp
        FROM    Wrogowie_Kocurow
        WHERE   pseudo = ps;
        
        IF temp = 0 THEN
          sumaPod := sumaPod + 1;
        END IF;
        
        SELECT  DECODE(plec,'M', 1,0)
        INTO    temp
        FROM    Kocury
        WHERE   pseudo = ps;
        
        IF temp = 1 THEN
          sumaPod := sumaPod + 1;
        END IF;
                
        RETURN sumaPod;
    END podatek;
    
    PROCEDURE nowaBanda(
            nrB       Bandy.nr_bandy%TYPE,
            nazwaB    Bandy.nazwa%TYPE,
            terenB    Bandy.teren%TYPE
        ) AS
        duplikat      BOOLEAN := FALSE;
        dup_values    EXCEPTION;
        less_zero     EXCEPTION;
        null_values   EXCEPTION;
        numNr         NUMBER;
        numNaz        NUMBER;
        numTer        NUMBER;
    BEGIN
        IF  nrB IS NOT NULL
        AND nrB <= 0 THEN
            RAISE less_zero;
        END IF;
        
        SELECT COUNT(*) INTO numNr  FROM Bandy WHERE nrB    = nr_bandy;
        SELECT COUNT(*) INTO numNaz FROM Bandy WHERE nazwaB = nazwa;
        SELECT COUNT(*) INTO numTer FROM Bandy WHERE terenB = teren;
        
        IF numNr > 0 THEN
            DBMS_OUTPUT.PUT(nrB || ', ');
            duplikat := TRUE;
        END IF;
        
        IF numNaz > 0 THEN
            DBMS_OUTPUT.PUT(nazwaB || ', ');
            duplikat := TRUE;
        END IF;
        
        IF numTer > 0 THEN
            DBMS_OUTPUT.PUT(terenB);
            duplikat := TRUE;
        END IF;
        
        IF duplikat THEN
            RAISE dup_values;
        END IF;
        
        IF  nazwaB  IS NULL 
        OR  terenB  IS NULL THEN
            RAISE null_values;
        END IF;
        
        INSERT INTO Bandy (nr_bandy, nazwa, teren) VALUES (nrB, nazwaB, terenB);
        DBMS_OUTPUT.PUT_LINE('Pola dodane pomyslnie !');
    EXCEPTION
        WHEN DUP_VALUES   THEN DBMS_OUTPUT.PUT_LINE(' :juz istnieje');
        WHEN LESS_ZERO    THEN DBMS_OUTPUT.PUT_LINE('Numer bandy <= 0');
        WHEN OTHERS       THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
    END nowaBanda;
END podatek_poglowny;
/

BEGIN
	DBMS_OUTPUT.PUT_LINE('PSEUDO    PODATEK');
	DBMS_OUTPUT.PUT_LINE('--------- ---------');
	FOR kot IN (SELECT pseudo FROM Kocury) LOOP
		DBMS_OUTPUT.PUT(RPAD(kot.pseudo, 10));
		DBMS_OUTPUT.PUT(podatek_poglowny.podatek(kot.pseudo));
		DBMS_OUTPUT.NEW_LINE();
	END LOOP;
END;
/

--Zadanie 45
DROP SEQUENCE indexDodatki;
DROP TABLE Dodatki_Extra;

CREATE SEQUENCE index_dodatki
START WITH 1
INCREMENT BY 1;

CREATE TABLE Dodatki_Extra(
    id NUMBER DEFAULT index_dodatki.nextval PRIMARY KEY,
	pseudo VARCHAR2(15) CONSTRAINT de_ps_fk REFERENCES Kocury(pseudo),
	dodatek NUMBER
);

CREATE OR REPLACE TRIGGER Antywirus
AFTER UPDATE OF przydzial_myszy, myszy_extra ON Kocury
FOR EACH ROW WHEN(OLD.funkcja = 'MILUSIA')
DECLARE
    ilosc       NUMBER := 0;
BEGIN
    IF (LOGIN_USER != 'TYGRYS' AND (:NEW.przydzial_myszy > :OLD.przydzial_myszy OR :NEW.myszy_extra > :OLD.myszy_extra)) THEN
        SELECT COUNT(*) INTO ilosc FROM Dodatki_extra WHERE pseudo = :OLD.pseudo;
        IF ilosc = 0 THEN
            INSERT INTO Dodatki_extra(pseudo, dodatek) VALUES(:OLD.pseudo, -10);
        ELSE
            UPDATE Dodatki_extra SET dodatek = dodatek - 10 WHERE pseudo = :OLD.pseudo;
        END IF;
    END IF;
END;
/

SELECT pseudo, funkcja, przydzial_myszy, myszy_extra FROM Kocury WHERE funkcja = 'MILUSIA' OR pseudo = 'TYGRYS';
UPDATE Kocury SET przydzial_myszy = przydzial_myszy + 1 WHERE pseudo = 'LASKA';
SELECT pseudo, funkcja, przydzial_myszy, myszy_extra FROM Kocury;
SELECT * FROM Dodatki_extra;
ROLLBACK;

--Zadanie 46
SELECT USER
FROM dual;

CREATE TABLE Logi(
  kto VARCHAR2(255),
  kiedy DATE,
  pseudo VARCHAR2(255),
  operacja VARCHAR2(255)
);

DROP TABLE Logi;

CREATE OR REPLACE TRIGGER Minmax_Logger
BEFORE UPDATE OR INSERT ON KOCURY
FOR EACH ROW
DECLARE
    min_f    Kocury.przydzial_myszy%TYPE;
    max_f    Kocury.przydzial_myszy%TYPE;
    op_type  VARCHAR2(10);
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    SELECT  min_myszy,  max_myszy
    INTO    min_f,      max_f
    FROM    Funkcje
    WHERE   funkcja = :NEW.funkcja;
    
    
    IF :NEW.PRZYDZIAL_MYSZY < min_f OR max_f < :NEW.PRZYDZIAL_MYSZY THEN
    
        IF INSERTING THEN
            op_type := 'INSERTING';
        ELSIF UPDATING THEN
            op_type := 'UPDATING';
        END IF;
    
        EXECUTE IMMEDIATE ( 'INSERT INTO Logi VALUES (:kto, :kiedy, :komu, :co)' )
            USING LOGIN_USER, SYSDATE, :new.pseudo, op_type;
        COMMIT;
        RAISE_APPLICATION_ERROR(-20020,'Przekroczony zakres myszy');
    END IF;
END;
/

CREATE OR REPLACE TRIGGER Minmax_Logger
BEFORE UPDATE OR INSERT ON KOCURY
FOR EACH ROW
DECLARE
    min_f    Kocury.przydzial_myszy%TYPE;
    max_f    Kocury.przydzial_myszy%TYPE;
    op_type  VARCHAR2(10);
BEGIN
    SELECT  min_myszy,  max_myszy
    INTO    min_f,      max_f
    FROM    Funkcje
    WHERE   funkcja = :NEW.funkcja;
    
    
    IF :NEW.przydzial_myszy < min_f OR max_f < :NEW.przydzial_myszy THEN
    
        IF INSERTING THEN
            op_type := 'INSERTING';
        ELSIF UPDATING THEN
            op_type := 'UPDATING';
        END IF;
        
        INSERT INTO Logi VALUES (USER, SYSDATE, :new.pseudo, op_type);
        :NEW.przydzial_myszy := :OLD.przydzial_myszy;
        --RAISE_APPLICATION_ERROR(-20020,'Przekroczony zakres myszy');
    END IF;
END;
/

UPDATE  Kocury 
SET     przydzial_myszy = 999
WHERE   pseudo = 'TYGRYS';

INSERT INTO Kocury(imie,plec,pseudo,funkcja,szef,w_stadku_od,przydzial_myszy,myszy_extra,nr_bandy) 
VALUES('MELA','D','DAMA2','LAPACZ','RAFA','2008-11-01',999,NULL,4);

SELECT * FROM Kocury WHERE pseudo = 'TYGRYS';
SELECT * FROM Logi;
DELETE FROM Logi;

DROP TRIGGER MINMAX_LOGGER;
